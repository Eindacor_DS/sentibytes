package com.sentibytes.attributes

enum class AttributeType
{
    PERSONAL, INTERPERSONAL
}