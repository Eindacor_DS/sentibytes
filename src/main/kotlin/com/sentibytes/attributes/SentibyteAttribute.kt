package com.sentibytes.attributes

import com.sentibytes.infrastructure.Collection
import com.sentibytes.infrastructure.DBKey
import com.sentibytes.infrastructure.DataAccess
import com.sentibytes.infrastructure.DocumentBuilder
import org.bson.Document

enum class SentibyteAttribute(val min: Double, val max: Double, val type: AttributeType)
{
    REGARD(.1, 1.0, AttributeType.PERSONAL),
    VOLATILITY(.5, 1.0, AttributeType.PERSONAL),
    SENSITIVITY(.05, .2, AttributeType.PERSONAL),
    OBSERVANT(.7, 1.0, AttributeType.PERSONAL),
    PICKINESS(.60, .90, AttributeType.PERSONAL),
    IMPRESSIONABILITY(.1, .15, AttributeType.PERSONAL),
    TOLERANCE(.1, 1.0, AttributeType.PERSONAL),
    POSITIVITY_RANGE(.1, .9, AttributeType.PERSONAL),
    ENERGY_RANGE(.1, .9, AttributeType.PERSONAL),
    ADVENTEROUS(.5, .2, AttributeType.PERSONAL),
    PRIVATE(.0, .75, AttributeType.PERSONAL),
    REFLECTIVE(.0, 1.0, AttributeType.PERSONAL),
    TRUSTING(.0, 1.0, AttributeType.PERSONAL),
    STAMINA(.5, .9, AttributeType.PERSONAL),
    INTELLIGENCE(.2, 1.0, AttributeType.PERSONAL),
    INQUISITIVE(0.0, 1.0, AttributeType.PERSONAL),
    SOCIABLE(.3, .9, AttributeType.PERSONAL),
    CONCUPISCENT(0.0, .5, AttributeType.PERSONAL),
    SELECTIVE(.9, .95, AttributeType.PERSONAL),

    POSITIVITY(0.0, 1.0, AttributeType.INTERPERSONAL),
    ENERGY(0.0, 1.0, AttributeType.INTERPERSONAL),
    TALKATIVE(0.0, 1.0, AttributeType.INTERPERSONAL),
    COMMUNICATIVE(0.0, 1.0, AttributeType.INTERPERSONAL),
    INTELLECTUAL(0.0, .5, AttributeType.INTERPERSONAL),
    CONFIDENT(0.0, .5, AttributeType.INTERPERSONAL),
    GOSSIPY(0.0, .5, AttributeType.INTERPERSONAL);

    companion object
    {
        private val ATTRIBUTE_TO_DB_ID = HashMap<SentibyteAttribute, String>()
        private val DB_ID_TO_ATTRIBUTE = HashMap<String, SentibyteAttribute>()

        init
        {
            DataAccess.fromCollection(Collection.ATTRIBUTES).find(Document()).forEach{
                DB_ID_TO_ATTRIBUTE[it!!.getString(DBKey.ID.key)] = valueOf(it.getString("name"))
            }
        }

        fun getDBId(sentibyteAttribute: SentibyteAttribute) : String
        {
            return ATTRIBUTE_TO_DB_ID.getOrElse(sentibyteAttribute, {
                SentibyteAttribute.values().forEach {
                    val queryDocument = DocumentBuilder("name", it.name).get()
                    ATTRIBUTE_TO_DB_ID.put(it, DataAccess.fromCollection(Collection.ATTRIBUTES).find(queryDocument).first()!!.getString(DBKey.ID.key))
                }

                return ATTRIBUTE_TO_DB_ID.get(sentibyteAttribute)!!
            })
        }

        fun getValueFromIdString(string: String) : SentibyteAttribute?
        {
            return DB_ID_TO_ATTRIBUTE[string]
        }
    }
}