package com.sentibytes.attributes

data class AttributeDesire(val priority: Int, val value: Double)