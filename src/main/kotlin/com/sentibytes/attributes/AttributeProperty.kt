package com.sentibytes.attributes

enum class AttributeProperty
{
    LOWER_BOUND, UPPER_BOUND, BASE_COEFFICIENT, FLUCTUATION, SENSITIVITY, CURRENT_VALUE;
}