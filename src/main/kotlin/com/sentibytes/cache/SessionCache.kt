package com.sentibytes.cache

import com.mongodb.BasicDBList
import com.sentibytes.infrastructure.Collection
import com.sentibytes.infrastructure.DataAccess
import com.sentibytes.infrastructure.DocumentBuilder
import com.sentibytes.session.Session
import com.sentibytes.session.SessionKey
import com.sentibytes.session.SessionType
import org.bson.types.ObjectId

class SessionCache private constructor() : DocumentCacheImpl<Session>({ id -> Session(id) })
{
    override fun unload(id: String)
    {
        cache.remove(id)
        DataAccess.fromCollection(Collection.SESSIONS).deleteOne(DocumentBuilder.id(id).get())
    }

    companion object
    {
        private var instance: SessionCache? = null

        fun get() : SessionCache
        {
            if (instance == null)
            {
                instance = SessionCache()
            }

            return instance!!
        }

        fun newSession(initiator: String, invitee: String, type: SessionType) : String
        {
            val sessionId = ObjectId().toString()
            val participants = BasicDBList()
            participants.add(initiator)
            participants.add(invitee)

            val newSessionDoc = DocumentBuilder.id(sessionId)
                    .put(SessionKey.SESSION_TYPE.name, type.name)
                    .put(SessionKey.PARTICIPANTS.name, participants)
                    .put(SessionKey.NEW_PARTICIPANTS.name, BasicDBList())
                    .put(SessionKey.LEAVING_PARTICIPANTS.name, BasicDBList())
                    .put(SessionKey.CYCLE_COUNT.name, 0)
                    .get()

            DataAccess.fromCollection(Collection.SESSIONS).insertOne(newSessionDoc)
            return sessionId
        }

    }
}