package com.sentibytes.cache

import com.sentibytes.infrastructure.DocumentBase
import com.sentibytes.infrastructure.DocumentCache

abstract class DocumentCacheImpl <T : DocumentBase> (private val constructor : (String) -> T) : DocumentCache<T>
{
    val cache: MutableMap<String, T> = HashMap()

    override fun getOrPut(id: String) : T
    {
        return cache.getOrPut(id){
           constructor.invoke(id)
        }
    }

    override fun get(id: String) : T?
    {
        return cache[id]
    }

    override fun getWithoutPut(id: String) : T
    {
        return cache[id]?: constructor.invoke(id)
    }

    override fun unload(id: String)
    {
        if (cache.containsKey(id))
        {
            cache[id]?.save()
            cache.remove(id)
        }
    }

    override fun unloadAll()
    {
        cache.forEach{
            it.value.save()
        }

        cache.clear()
    }
}