package com.sentibytes.cache

import com.sentibytes.Sentibyte

class SentibyteCache private constructor() : DocumentCacheImpl<Sentibyte>({id -> Sentibyte(id)})
{
    companion object
    {
        private var instance: SentibyteCache? = null

        fun get() : SentibyteCache
        {
            if (instance == null)
            {
                instance = SentibyteCache()
            }

            return instance!!
        }
    }
}