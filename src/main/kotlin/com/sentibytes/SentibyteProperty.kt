package com.sentibytes

enum class SentibyteProperty {
    NAME, AGE, PARENTS, CHILDREN, COMMUNITY_ID, FRIENDS, BONDS, ACCURATE_KNOWLEDGE, FALSE_KNOWLEDGE, ATTRIBUTES, CONTACTS, PERCEPTIONS, DEATH_COEFFICIENT, CYCLES_BONDED, SOCIAL_COOLDOWN, DESIRED_ATTRIBUTES, CURRENT_SESSION;
}