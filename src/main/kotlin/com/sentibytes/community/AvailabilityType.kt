package com.sentibytes.community

enum class AvailabilityType
{
    IN_FULL_SESSION,
    IN_SESSION,
    ALONE
}