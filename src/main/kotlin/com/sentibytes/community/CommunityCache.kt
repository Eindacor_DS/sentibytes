package com.sentibytes.community

import com.sentibytes.infrastructure.Collection
import com.sentibytes.infrastructure.DataAccess

class CommunityCache
{
    companion object
    {
        fun get(communityId: String) : Community
        {
            return Community(DataAccess.get(communityId, Collection.COMMUNITIES)!!)
        }
    }
}