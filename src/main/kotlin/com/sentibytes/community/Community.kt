package com.sentibytes.community

import com.mongodb.BasicDBList
import com.mongodb.BasicDBObject
import com.mongodb.BasicDBObjectBuilder
import com.mongodb.DBObject
import com.mongodb.client.MongoCollection
import com.sentibytes.Sentibyte
import com.sentibytes.SentibyteProperty
import com.sentibytes.attributes.AttributeDesire
import com.sentibytes.attributes.AttributeProperty
import com.sentibytes.attributes.AttributeType
import com.sentibytes.attributes.SentibyteAttribute
import com.sentibytes.cache.SentibyteCache
import com.sentibytes.cache.SessionCache
import com.sentibytes.infrastructure.*
import com.sentibytes.infrastructure.Collection
import com.sentibytes.util.FluctuatingValue
import com.sentibytes.util.Util
import org.bson.Document
import org.bson.types.ObjectId
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.collections.HashSet

class Community(document: Document) : DocumentBase(document, Collection.COMMUNITIES)
{
    val communityId: String = documentId

    val maxChildren: Int = getValue(CommunityKey.MAX_CHILDREN.name)!!

    val childAge: Int = getValue(CommunityKey.CHILD_AGE.name)!!

    private var members : MutableSet<String> = HashSet()

    private var alone : MutableSet<String> = HashSet()

    val communicationsPerCycle : Int = getValue(CommunityKey.COMMUNICATIONS_PER_CYCLE.name)!!

    private var chunkCursor : Int = 0

    val chunkSize : Int = 50

    companion object
    {
        private val communityCache: MutableMap<String, Community> = HashMap()

        private const val DEATH_COEFFICIENT = 0.0001

        fun getCommunity(communityId: String) : Community
        {
            return communityCache.getOrPut(communityId){
                Community(DataAccess.get(communityId, Collection.COMMUNITIES)!!)
            }
        }


    }

    fun updateMembers()
    {
        members = DataAccess.fromCollection(Collection.SENTIBYTES)
                .find(DocumentBuilder().put(SentibyteProperty.COMMUNITY_ID.name, communityId).get())
                .map { document -> document.getString(DBKey.ID.key) }
                .toMutableSet()

        alone = DataAccess.fromCollection(Collection.SENTIBYTES)
                .find(DocumentBuilder().put(SentibyteProperty.COMMUNITY_ID.name, communityId).put(SentibyteProperty.CURRENT_SESSION.name, null).get())
                .map { document -> document.getString(DBKey.ID.key) }
                .toMutableSet()
    }

    override fun pack(): Document
    {
        TODO()
    }

    override fun save()
    {
        writeToDB()
    }

    fun createSentibyte(motherId: String, fatherId: String) : String
    {
        val mother = SentibyteCache.get().getOrPut(motherId)
        val father = SentibyteCache.get().getOrPut(fatherId)

        val parents = BasicDBList()
        parents.addAll(listOf(motherId, fatherId))

        val id = ObjectId().toString()
        val sentibyte = DocumentBuilder.id(id)
                .put(SentibyteProperty.NAME.name, UUID.randomUUID().toString())
                .put(SentibyteProperty.AGE.name, 0)
                // TODO refactor families to keep track of removed status (immediate family = 0, cousins = 1, etc.)
                .put(SentibyteProperty.PARENTS.name, parents)
                .put(SentibyteProperty.CHILDREN.name, BasicDBList())
                .put(SentibyteProperty.COMMUNITY_ID.name, communityId)
                .put(SentibyteProperty.FRIENDS.name, BasicDBList())
                .put(SentibyteProperty.BONDS.name, BasicDBObject())
                .put(SentibyteProperty.CURRENT_SESSION.name, null)
                // TODO getOrPut some knowledge from parents
                .put(SentibyteProperty.ACCURATE_KNOWLEDGE.name, BasicDBList())
                .put(SentibyteProperty.FALSE_KNOWLEDGE.name, BasicDBObject())
                .put(SentibyteProperty.CONTACTS.name, BasicDBList())
                .put(SentibyteProperty.ATTRIBUTES.name, mergeAttributes(mother.attributes, father.attributes))
                .put(SentibyteProperty.DESIRED_ATTRIBUTES.name, mergeDesiredAttributes(mother.desiredAttributes, father.desiredAttributes))
                .put(SentibyteProperty.DEATH_COEFFICIENT.name, DEATH_COEFFICIENT)
                // TODO make social cooldown smarter, combine parents
                .put(SentibyteProperty.SOCIAL_COOLDOWN.name, 20 + Random().nextInt(20))
                .get()

        DataAccess.fromCollection(Collection.SENTIBYTES).insertOne(sentibyte)

        mother.addChild(id)
        father.addChild(id)

        return id
    }

    fun mergeAttributes(motherAttributes: Map<SentibyteAttribute, FluctuatingValue>, fatherAttributes: Map<SentibyteAttribute, FluctuatingValue>) : Document
    {
        val merged = DocumentBuilder()
        motherAttributes.forEach{
            val attribute = it.key
            val lowerBound = getCombinedAttribute(it.value.lowerBound, fatherAttributes[attribute]!!.lowerBound)
            val upperBound = getCombinedAttribute(it.value.upperBound, fatherAttributes[attribute]!!.upperBound)
            val baseCoefficient = getCombinedAttribute(it.value.baseCoefficient, fatherAttributes[attribute]!!.baseCoefficient)
            val fluctuation = getCombinedAttribute(it.value.fluctuationCoefficient, fatherAttributes[attribute]!!.fluctuationCoefficient)
            val sensitivity = getCombinedAttribute(it.value.sensitivity, fatherAttributes[attribute]!!.sensitivity)
            val current = Util.getValueFromRange(lowerBound, upperBound, baseCoefficient)

            merged.put(SentibyteAttribute.getDBId(attribute), DocumentBuilder()
                    .put(AttributeProperty.LOWER_BOUND.name, lowerBound)
                    .put(AttributeProperty.UPPER_BOUND.name, upperBound)
                    .put(AttributeProperty.BASE_COEFFICIENT.name, baseCoefficient)
                    .put(AttributeProperty.FLUCTUATION.name, fluctuation)
                    .put(AttributeProperty.SENSITIVITY.name, sensitivity)
                    .put(AttributeProperty.CURRENT_VALUE.name, current)
                    .get()
            )
        }

        return merged.get()
    }

    private fun mergeDesiredAttributes(motherDesiredAttributes: Map<SentibyteAttribute, AttributeDesire>, fatherDesiredAttributes: Map<SentibyteAttribute, AttributeDesire>) : Document
    {
        val merged = DocumentBuilder()
        motherDesiredAttributes.forEach{
            val attribute = it.key
            merged.put(SentibyteAttribute.getDBId(attribute), DocumentBuilder()
                    .put("priority", it.value.priority)
                    .put("value", getCombinedAttribute(it.value.value, fatherDesiredAttributes[attribute]!!.value))
                    .get()
            )
        }

        return merged.get()
    }

    private fun getCombinedAttribute(value1: Double, value2: Double): Double
    {
        // trait is always at least 90% shifted towards one parent
        var traitShift = .9
        var weightCoefficient = traitShift + (Random().nextDouble() * (1 - traitShift))
        // randomly flip between mother and father
        weightCoefficient = if (Random().nextBoolean()) weightCoefficient else 1 - weightCoefficient

        return Util.getValueFromRange(value1, value2, weightCoefficient)
    }

    fun createSentibyte() : String
    {
        val attributes = DataAccess.fromCollection(Collection.ATTRIBUTES)
        val interpersonalFilter = DocumentBuilder("type", AttributeType.INTERPERSONAL.name).get()

        val id = ObjectId().toString()
        val sentibyte = DocumentBuilder.id(id)
                .put(SentibyteProperty.NAME.name, UUID.randomUUID().toString())
                .put(SentibyteProperty.AGE.name, 0)
                .put(SentibyteProperty.PARENTS.name, BasicDBList())
                .put(SentibyteProperty.CHILDREN.name, BasicDBList())
                .put(SentibyteProperty.COMMUNITY_ID.name, communityId)
                .put(SentibyteProperty.FRIENDS.name, BasicDBList())
                .put(SentibyteProperty.BONDS.name, BasicDBObject())
                .put(SentibyteProperty.CURRENT_SESSION.name, null)
                .put(SentibyteProperty.ACCURATE_KNOWLEDGE.name, BasicDBList())
                .put(SentibyteProperty.FALSE_KNOWLEDGE.name, BasicDBObject())
                .put(SentibyteProperty.CONTACTS.name, BasicDBList())
                .put(SentibyteProperty.ATTRIBUTES.name, getSentibyteAttributes(attributes))
                .put(SentibyteProperty.DESIRED_ATTRIBUTES.name, getSentibyteDesiredAttributes(attributes.find(interpersonalFilter).toList()))
                .put(SentibyteProperty.DEATH_COEFFICIENT.name, DEATH_COEFFICIENT)
                .put(SentibyteProperty.SOCIAL_COOLDOWN.name, 20 + Random().nextInt(20))
                .get()

        DataAccess.fromCollection(Collection.SENTIBYTES).insertOne(sentibyte)

        return id
    }

    private fun getSentibyteDesiredAttributes(interpersonalAttributes : List<Document>) : DBObject
    {
        val desiredAttributes = BasicDBObjectBuilder()

        val shuffledInterpersonalAttributes = interpersonalAttributes.toMutableList().shuffled()

        var priority = 1;

        shuffledInterpersonalAttributes.forEach {
            if (it.getString("type").equals(AttributeType.INTERPERSONAL.name)) {
                val desiredAttribute = DocumentBuilder("priority", priority++)
                        .put("value", Random().nextDouble())
                        .get()

                desiredAttributes.add(it.getString(DBKey.ID.key).toString(), desiredAttribute)
            }
        }

        return desiredAttributes.get()
    }

    private fun getSentibyteAttributes(attributes: MongoCollection<Document>) : Document
    {
        val sentibyteAttributes = DocumentBuilder()

        attributes.find(Document()).forEach{
            sentibyteAttributes.put(it.get("_id").toString(), getAttributeDocument(it))
        }

        return sentibyteAttributes.get()
    }

    private fun getAttributeDocument(attributeDocument: Document) : DBObject
    {
        // standard buffer between lower and upper bounds
        val boundBufferCoefficient = 0.1f
        val attributeMin = attributeDocument.getDouble("min")
        val attributeMax = attributeDocument.getDouble("max")
        val range = attributeMax - attributeMin
        val boundRange = (range * (1 - boundBufferCoefficient)) / 2

        val maximumLowerBound = attributeMin + boundRange
        val minimumUpperBound = attributeMax - boundRange

        val lowerBound = Util.getValueFromRange(attributeMin, maximumLowerBound, Random().nextDouble())
        val upperBound = Util.getValueFromRange(minimumUpperBound, attributeMax, Random().nextDouble())

        return BasicDBObjectBuilder()
                .add(AttributeProperty.LOWER_BOUND.name, lowerBound)
                .add(AttributeProperty.UPPER_BOUND.name, upperBound)
                .add(AttributeProperty.BASE_COEFFICIENT.name, Random().nextDouble())
                .add(AttributeProperty.FLUCTUATION.name, Util.getValueFromRange(.02, .05, Random().nextDouble()))
                .add(AttributeProperty.SENSITIVITY.name, Util.getValueFromRange(0.2, 0.5, Random().nextDouble()))
                .add(AttributeProperty.CURRENT_VALUE.name, Util.getValueFromRange(lowerBound, upperBound, .5))
                .get()
    }


    fun cycle()
    {
        updateMembers()

        var aloneChunk = getAloneChunk()

        while (aloneChunk != null)
        {
            aloneChunk.forEach{
                val cycle = Sentibyte.getCycle(it)
                cycle.run(it)
            }

            aloneChunk = getAloneChunk()
            SentibyteCache.get().unloadAll()
        }

        val toUnload : MutableList<String> = ArrayList()
        SessionCache.get().cache.forEach{
            val session = it.value
            session.cycle()
            if (session.participants.size < 2)
            {
                it.value.participants.forEach{
                    session.endSession()
                }
                toUnload.add(it.key)
            }
        }

        toUnload.forEach{
            SessionCache.get().unload(it)
        }
    }

    fun getAloneChunk() : List<String>?
    {
        if (members.size < chunkCursor)
        {
            chunkCursor = 0;
            return null
        }

        val toIndex = Math.min(chunkCursor + chunkSize, members.size)
        val chunk = members.toList().subList(chunkCursor, toIndex)
        chunkCursor += chunkSize
        return chunk
    }
}