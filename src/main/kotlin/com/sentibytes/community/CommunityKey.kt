package com.sentibytes.community

enum class CommunityKey
{
    //TODO come up with new name for child
    MAX_CHILDREN, CHILD_AGE, COMMUNICATIONS_PER_CYCLE
}