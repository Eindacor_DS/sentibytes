package com.sentibytes.communication

enum class TransmissionType {
    STATEMENT, SIGNAL
}