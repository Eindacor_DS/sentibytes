package com.sentibytes.communication

import com.sentibytes.attributes.AttributeType
import com.sentibytes.attributes.SentibyteAttribute
import com.sentibytes.infrastructure.Collection
import com.sentibytes.infrastructure.DataAccess
import com.sentibytes.infrastructure.DocumentBase
import com.sentibytes.infrastructure.DocumentBuilder
import com.sentibytes.util.AverageContainer
import org.bson.Document
import org.bson.types.ObjectId

class Interaction constructor(document: Document): DocumentBase(document, Collection.INTERACTIONS)
{
    val ownerId: String = getValue(InteractionKey.OWNER_ID.name)!!

    val otherId: String = getValue(InteractionKey.OTHER_ID.name)!!

    private var closed: Boolean = getValue(InteractionKey.CLOSED.name)!!

    enum class CountKey {ALL, STATEMENT, KNOWLEDGE, GOSSIP, BRAG}
    enum class AverageKey {POSITIVITY, ENERGY}
    enum class AudienceKey { OWNER, OTHERS, TOTAL }

    val traitGuesses = getHashMap(InteractionKey.TRAIT_GUESSES.name, {key -> SentibyteAttribute.getValueFromIdString(key)!!}, {value -> value as Double})

    // HashMap<AudienceKey, Map<CountKey, Int>>()
    private val counts = getHashMap(InteractionKey.COUNTS.name,
        {key -> AudienceKey.valueOf(key)},
        {value ->
            val countDoc = value as Document
            val countMap : MutableMap<CountKey, Int> = HashMap()
            CountKey.values().forEach {
                countMap[it] = countDoc[it.name] as Int
            }
            HashMap(countMap)
        })

    private val averages = getHashMap(InteractionKey.AVERAGES.name,
            {key -> AudienceKey.valueOf(key)},
            {value ->
                val averagesDoc = value as Document
                val averagesMap : MutableMap<AverageKey, AverageContainer> = HashMap()
                AverageKey.values().forEach {
                    // unpacking disregards previous counts, as no new transmissions can be added to memories
                    val averageContainer = AverageContainer()
                    averageContainer.addValue(averagesDoc[it.name] as Double)
                    averagesMap[it] = averageContainer
                }
                HashMap(averagesMap)
            })

    companion object
    {
        fun getInteractions(ownerId: String, otherId: String) : List<Interaction>
        {
            val query = DocumentBuilder(InteractionKey.OWNER_ID.name, ownerId)
                    .put(InteractionKey.OTHER_ID.name, otherId)
                    .get()
            val collection = DataAccess.fromCollection(Collection.INTERACTIONS)
            return collection.find(query).map{document -> Interaction(document)}.toList()
        }

        fun newInteraction(ownerId: String, otherId: String) : Interaction
        {
            val overallCountsDoc = DocumentBuilder()
            val overallAveragesDoc = DocumentBuilder()
            AudienceKey.values().forEach{
                val audienceKey = it;
                val countsDoc = DocumentBuilder()
                CountKey.values().forEach {
                    val countKey = it
                    countsDoc.put(countKey.name, 0)
                }
                overallCountsDoc.put(audienceKey.name, countsDoc)

                val averagesDoc = DocumentBuilder()
                AverageKey.values().forEach{
                    val averageKey = it
                    averagesDoc.put(averageKey.name, 0.0)
                }
                overallAveragesDoc.put(audienceKey.name, averagesDoc)
            }

            val traitGuessesDoc = DocumentBuilder()
            SentibyteAttribute.values().filter{sentibyteAttribute -> sentibyteAttribute.type.equals(AttributeType.INTERPERSONAL) }.forEach{
                val attributeId = SentibyteAttribute.getDBId(it)
                traitGuessesDoc.put(attributeId.toString(), 0.0)
            }

            val interactionDoc = DocumentBuilder.id(ObjectId().toString())
                    .put(InteractionKey.OWNER_ID.name, ownerId)
                    .put(InteractionKey.OTHER_ID.name, otherId)
                    .put(InteractionKey.COUNTS.name, overallCountsDoc)
                    .put(InteractionKey.AVERAGES.name, overallAveragesDoc)
                    .put(InteractionKey.TRAIT_GUESSES.name, traitGuessesDoc)
                    .put(InteractionKey.CLOSED.name, false)
                    .get()

            DataAccess.fromCollection(Collection.INTERACTIONS).insertOne(interactionDoc)
            return Interaction(interactionDoc)
        }
    }

    override fun pack(): Document
    {
        val overallCountsDoc = DocumentBuilder()
        val overallAveragesDoc = DocumentBuilder()
        AudienceKey.values().forEach{
            val audienceKey = it;
            val countsDoc = DocumentBuilder()
            CountKey.values().forEach {
                val countKey = it
                countsDoc.put(countKey.name, counts[audienceKey]!![countKey]!!)
            }
            overallCountsDoc.put(audienceKey.name, countsDoc)

            val averagesDoc = DocumentBuilder()
            AverageKey.values().forEach{
                val averageKey = it
                averagesDoc.put(averageKey.name, averages[audienceKey]!![averageKey]!!.average)
            }
            overallAveragesDoc.put(audienceKey.name, averagesDoc)
        }

        val traitGuessesDoc = DocumentBuilder()
        traitGuesses.forEach{
            val attributeId = SentibyteAttribute.getDBId(it.key)
            traitGuessesDoc.put(attributeId, it.value)
        }

        return DocumentBuilder(InteractionKey.COUNTS.name, overallCountsDoc)
                .put(InteractionKey.AVERAGES.name, overallAveragesDoc)
                .put(InteractionKey.TRAIT_GUESSES.name, traitGuessesDoc)
                .put(InteractionKey.CLOSED.name, closed)
                .get()
    }

    override fun save()
    {
        // TODO look into locking interactions down, where to put
        //closed = true;
        writeToDB()
    }

    fun addTransmission(transmission: Transmission)
    {
        if (closed)
        {
            return
        }

        // "others", "audience", "owner"
        val audience = HashSet<AudienceKey>()
        audience.add(AudienceKey.TOTAL)

        if (ownerId !in transmission.targets)
        {
            audience.add(AudienceKey.OTHERS)
        }
        else if (transmission.targets.size == 1)
        {
            audience.add(AudienceKey.OWNER)
        }

        audience.forEach{
            averages[it]!![AverageKey.ENERGY]!!.addValue(transmission.energy)
            averages[it]!![AverageKey.POSITIVITY]!!.addValue(transmission.positivity)

            if (transmission.transmissionType == TransmissionType.STATEMENT)
            {
                counts[it]!![CountKey.STATEMENT] = counts[it]!![CountKey.STATEMENT]!! + 1
            }

            if (transmission.knowledge != null && transmission.falseKnowledge != null)
            {
                counts[it]!![CountKey.KNOWLEDGE] = counts[it]!![CountKey.KNOWLEDGE]!! + 1
            }

            if (transmission.gossip != null)
            {
                counts[it]!![CountKey.GOSSIP] = counts[it]!![CountKey.GOSSIP]!! + 1
            }

            if (transmission.brag != null)
            {
                counts[it]!![CountKey.BRAG] = counts[it]!![CountKey.BRAG]!! + 1
            }

            counts[it]!![CountKey.ALL] = counts[it]!![CountKey.ALL]!! + 1
        }

        modified = true
    }

    fun updateGuesses(cyclesInSession: Int, communicationsPerCycle: Int, startCycle: Int = 0)
    {
        val cyclesPresent = cyclesInSession - startCycle

        val countTotals = counts[AudienceKey.TOTAL]!!

        val totalCommunicationsObserved = countTotals[CountKey.ALL]!!
        val totalStatementsObserved = countTotals[CountKey.STATEMENT]!!
        val totalKnowledgeableObserved = countTotals[CountKey.KNOWLEDGE]!!
        val totalGossipObserved = countTotals[CountKey.GOSSIP]!!
        val totalBragsObserved = countTotals[CountKey.BRAG]!!

        // communicative
        // reverse-engineer by comparing total communications to potential communication ALL
        val maxCommunications : Double = cyclesPresent * communicationsPerCycle.toDouble()
        val communicativeGuess : Double = totalCommunicationsObserved / maxCommunications
        traitGuesses[SentibyteAttribute.COMMUNICATIVE] = Math.min(communicativeGuess, 1.0)

        // talkative
        if (totalCommunicationsObserved > 0)
        {
            val talkativeGuess = totalStatementsObserved.toDouble() / totalCommunicationsObserved.toDouble()
            traitGuesses[SentibyteAttribute.TALKATIVE] = Math.min(talkativeGuess, 1.0)
        }

        if (totalStatementsObserved > 0)
        {
            // intellectual
            val intellectualGuess = totalKnowledgeableObserved.toDouble() / totalStatementsObserved.toDouble()
            traitGuesses[SentibyteAttribute.INTELLECTUAL] = Math.min(intellectualGuess, 1.0)

            // gossipy
            val gossipyGuess = totalGossipObserved.toDouble() / totalCommunicationsObserved.toDouble()
            traitGuesses[SentibyteAttribute.GOSSIPY] = Math.min(gossipyGuess, 1.0)

            // confident
            val confidentGuess = totalStatementsObserved.toDouble() / totalBragsObserved.toDouble()
            traitGuesses[SentibyteAttribute.CONFIDENT] = Math.min(confidentGuess, 1.0)
        }

        traitGuesses[SentibyteAttribute.POSITIVITY] = averages[AudienceKey.TOTAL]!![AverageKey.POSITIVITY]!!.average
        traitGuesses[SentibyteAttribute.ENERGY] = averages[AudienceKey.TOTAL]!![AverageKey.ENERGY]!!.average
        modified=true
    }
}