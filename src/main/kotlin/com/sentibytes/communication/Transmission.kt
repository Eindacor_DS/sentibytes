package com.sentibytes.communication

import com.sentibytes.attributes.SentibyteAttribute

data class Transmission(val sourceId : String,
                        val targets : List<String>,
                        val positivity : Double,
                        val energy : Double,
                        val transmissionType: TransmissionType,
                        val knowledge : MutableSet<String>? = null,
                        val falseKnowledge: MutableMap<String, String>? = null,
                        val gossip : Interaction? = null,
                        val brag : Pair<SentibyteAttribute, Double>? = null)