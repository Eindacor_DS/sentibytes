package com.sentibytes.communication

enum class InteractionType
{
    MEMORY, RUMOR, IN_PERSON
}