package com.sentibytes.communication

enum class InteractionKey
{
    OWNER_ID,
    OTHER_ID,
    COUNTS,
    AVERAGES,
    POSITIVITY,
    ENERGY,
    TRAIT_GUESSES,
    ATTRIBUTE_ID,
    CLOSED
}