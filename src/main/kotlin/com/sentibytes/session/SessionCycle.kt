package com.sentibytes.session

import com.sentibytes.attributes.SentibyteAttribute
import com.sentibytes.cache.SentibyteCache
import com.sentibytes.cache.SessionCache
import com.sentibytes.communication.Transmission
import com.sentibytes.community.CommunityCache
import com.sentibytes.metadata.SentibyteMetadataCategory

class SessionCycle : Cycle
{
    override fun run(sentibyteId : String)
    {
        val sentibyte = SentibyteCache.get().getOrPut(sentibyteId)

        sentibyte.update()
        val sessionId = sentibyte.currentSession

        if (sessionId == null)
        {
            println("session cycle run on sessionless sentibyte: ${sentibyteId}")
            return
        }

        val session = SessionCache.get().getOrPut(sessionId)
        if (session.participants.size < session.sessionType.maxParticipants && sentibyte.proc(SentibyteAttribute.SOCIABLE))
        {
            val toIncrement = if (sentibyte.sendInvitation()) SentibyteMetadataCategory.SUCCESSFUL_CONNECTION_ATTEMPTS else SentibyteMetadataCategory.FAILED_CONNECTION_ATTEMPTS
            sentibyte.incrementMetadata(toIncrement)
        }

        val availableTargets = session.participants.filter { it != sentibyte.sentibyteId }
        val transmissions : MutableList<Transmission> = ArrayList()
        val communicationsPerCycle = CommunityCache.get(sentibyte.communityId!!).communicationsPerCycle
        for (i in 0 until communicationsPerCycle)
        {
            transmissions.add(sentibyte.broadcast(availableTargets))
        }

        session.distributeTransmissions(transmissions)

        // if new participants are present, stay to promote interactions
        if (session.newParticipants.isEmpty() && !sentibyte.proc(SentibyteAttribute.STAMINA))
        {
            session.leaveSession(sentibyteId)
        }

        sentibyte.procHealth()
    }
}