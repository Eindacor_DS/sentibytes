package com.sentibytes.session

import com.sentibytes.Sentibyte
import com.sentibytes.cache.SentibyteCache
import com.sentibytes.communication.Transmission
import com.sentibytes.infrastructure.Collection
import com.sentibytes.infrastructure.DataAccess
import com.sentibytes.infrastructure.DocumentBase
import com.sentibytes.infrastructure.DocumentBuilder
import com.sentibytes.metadata.SentibyteMetadataCategory
import org.bson.Document

class Session(document: Document) : DocumentBase(document, Collection.SESSIONS)
{
    val sessionId = documentId

    val sessionType: SessionType = SessionType.valueOf(getValue<String>(SessionKey.SESSION_TYPE.name)!!)

    val participants : MutableSet<String> = getSet(SessionKey.PARTICIPANTS.name)

    var newParticipants : MutableSet<String> = getSet(SessionKey.NEW_PARTICIPANTS.name)
        private set

    var leavingParticipants: MutableList<String> = getList(SessionKey.LEAVING_PARTICIPANTS.name)

//    val membersPerSession = AverageContainer()
//
//    private var mostPopularSession : Int = 0

    var cycleCount: Int = getValue(SessionKey.CYCLE_COUNT.name)!!
        private set

    constructor(id: String) : this(DataAccess.get(id, Collection.SESSIONS)!!)

    override fun pack(): Document
    {
        return DocumentBuilder()
                .put(SessionKey.PARTICIPANTS.name, participants)
                .put(SessionKey.NEW_PARTICIPANTS.name, newParticipants)
                .put(SessionKey.LEAVING_PARTICIPANTS.name, leavingParticipants)
                .put(SessionKey.CYCLE_COUNT.name, cycleCount)
                .get()
    }

    override fun save()
    {
        writeToDB()
    }

    fun distributeTransmissions(transmissions: List<Transmission>)
    {
        participants.forEach{
            val participant = SentibyteCache.get().getOrPut(it)
            transmissions.forEach{
                participant.exposeToTransmission(it)
            }
        }
    }

    fun endSession()
    {
        participants.forEach{
            val sentibyte = SentibyteCache.get().getWithoutPut(it)
            sentibyte.currentSession = null
            sentibyte.save()
        }
    }

    fun cycle()
    {
        cycleCount++

        //TODO refactor
        val participantList = participants.toList()
        for (i in 0 until participantList.size)
        {
            val cycle = Sentibyte.getCycle(participantList[i])
            cycle.run(participantList[i])
        }

        for (i in 0 until leavingParticipants.size)
        {
            participants.remove(leavingParticipants[i])
            val leaving = SentibyteCache.get().getOrPut(leavingParticipants[i])
            leaving.currentSession = null
            leaving.socialCooldown = 20
        }

        leavingParticipants.clear()
        newParticipants.clear()
        SentibyteCache.get().unloadAll()
        save()
    }

    fun addParticipant(id: String, inviterId: String? = null)
    {
        participants.add(id)
        newParticipants.add(id)

        SentibyteCache.get().getOrPut(id).currentSession = sessionId

        // add id's to participants

        participants.forEach{
            val participant = SentibyteCache.get().getOrPut(it)
            if (id !in SentibyteCache.get().getOrPut(it).contacts)
            {
                val toIncrement = if (inviterId != null) SentibyteMetadataCategory.MET_THROUGH_OTHERS else SentibyteMetadataCategory.MET_ON_OWN
                participant.incrementMetadata(toIncrement)
            }
        }
    }

    fun leaveSession(id: String)
    {
        leavingParticipants.add(id)
    }

    private fun cleanup()
    {
        leavingParticipants.forEach{
            participants.remove(it)
            val leaving = SentibyteCache.get().getOrPut(it)
            leaving.currentSession = null
            leaving.socialCooldown = 20
        }

        leavingParticipants.clear()
        newParticipants.clear()
        SentibyteCache.get().unloadAll()
        save()
    }
}