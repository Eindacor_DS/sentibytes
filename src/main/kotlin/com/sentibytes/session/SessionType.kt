package com.sentibytes.session

enum class SessionType(val maxParticipants: Int)
{
    PERSONAL(2), SMALL(4), MEDIUM(10), LARGE(20)
}