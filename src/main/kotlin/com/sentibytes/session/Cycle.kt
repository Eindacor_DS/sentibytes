package com.sentibytes.session

interface Cycle
{
    fun run(sentibyteId: String)
}