package com.sentibytes.session

enum class SessionKey
{
    SESSION_TYPE,
    PARTICIPANTS,
    NEW_PARTICIPANTS,
    LEAVING_PARTICIPANTS,
    MEMBERS_PER_SESSION,
    MOST_POPULAR_SESSION,
    CYCLE_COUNT
//    val sessionId = documentId
//
//    val sessionType: SessionType = fromString(SessionKey)
//
//    val participants : MutableSet<String> = HashSet()
//
//    var newParticipants : MutableSet<String> = HashSet()
//        private set
//
//    var leavingParticipants: MutableSet<String> = HashSet()
//
//    val membersPerSession = AverageContainer()
//
//    private var mostPopularSession : Int = 0
//
//    var cycleCount = 0
}