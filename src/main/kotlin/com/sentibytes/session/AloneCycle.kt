package com.sentibytes.session

import com.sentibytes.Sentibyte
import com.sentibytes.attributes.SentibyteAttribute
import com.sentibytes.cache.SentibyteCache
import com.sentibytes.community.Community
import com.sentibytes.metadata.SentibyteMetadataCategory

class AloneCycle : Cycle
{
    override fun run(sentibyteId: String)
    {
        // TODO force young sentibytes to interact with parents

        val sentibyte = SentibyteCache.get().getOrPut(sentibyteId)

        if (!sentibyte.procHealth())
        {
            return
        }

        sentibyte.update()

        if (sentibyte.proc(SentibyteAttribute.INTELLECTUAL))
        {
            sentibyte.learn()
        }

        if (sentibyte.proc((SentibyteAttribute.REFLECTIVE)))
        {
            sentibyte.reflect()
        }

        sentibyte.bonds.forEach{
            sentibyte.bonds[it.key] = it.value + 1
        }

        if (sentibyte.age < Community.getCommunity(sentibyte.communityId!!).childAge && sentibyte.proc(SentibyteAttribute.CONCUPISCENT))
        {
            val potentialBonds = sentibyte.bonds.filter { bond -> sentibyte.wantsToBond(bond.key) == Sentibyte.BondResponse.YES }.toList().toMutableList()
            if (potentialBonds.isNotEmpty())
            {
                // TODO refactor to select based on rating
                potentialBonds.shuffle()
                potentialBonds.forEach{
                    sentibyte.attemptBond(it.first)
                }
            }
        }

        if (sentibyte.socialCooldown == 0)
        {
            if (sentibyte.proc(SentibyteAttribute.SOCIABLE))
            {
                val toIncrement = if (sentibyte.sendInvitation())
                {
                    SentibyteMetadataCategory.SUCCESSFUL_CONNECTION_ATTEMPTS
                }
                else
                {
                    SentibyteMetadataCategory.FAILED_CONNECTION_ATTEMPTS
                }

                sentibyte.incrementMetadata(toIncrement)
            }
        }
        else
        {
            sentibyte.tickCooldown()
        }

        SentibyteCache.get().unload(sentibyteId)
    }
}