package com.sentibytes.util

import java.util.*

class FluctuatingValue(val lowerBound: Double, val upperBound: Double, val baseCoefficient: Double, val fluctuationCoefficient: Double, val sensitivity : Double, private var currentValue: Double)
{
    var baseValue : Double = 0.0
        private set

    private val POSITIVE_CHANCE_COEFFICIENT: Double = .5

    private val MAX_VALUE: Double = 1.0

    private val MIN_VALUE: Double = 0.0

    companion object
    {
        private val currentCoefficient = .1
        private val currentMaxOffset = .1
    }

    init
    {
        val delta = upperBound - lowerBound
        baseValue = (delta * baseCoefficient) + lowerBound
    }

    fun influence(influenceValue: Double, coefficient : Double)
    {
        val valueChange = calcInfluence(currentValue, influenceValue, coefficient)
        currentValue += valueChange
    }

    private fun calcInfluence(startingValue: Double, influenceValue: Double, coefficient: Double) : Double
    {
        val delta = influenceValue - startingValue
        return delta * coefficient
    }

    fun fluctuate()
    {
        val positiveChance = getPositiveChance()
        var valueChange = fluctuationCoefficient * (upperBound - lowerBound)

        if (Random().nextDouble() < positiveChance) {
            valueChange *= -1
        }

        currentValue = Math.min(Math.max(currentValue + valueChange, MIN_VALUE), MAX_VALUE)
    }

    fun getRelative() : Double
    {
        return (currentValue - lowerBound) / (upperBound - lowerBound)
    }

    private fun getPositiveChance() : Double {
        if (currentValue >= baseValue) {
            val distanceFromUpper = upperBound - currentValue
            val upperRange = upperBound - baseValue
            return distanceFromUpper / upperRange * POSITIVE_CHANCE_COEFFICIENT
        }
        else {
            val distanceFromLower = currentValue - lowerBound
            val lowerRange = baseValue - lowerBound
            return 1 - (distanceFromLower / lowerRange * POSITIVE_CHANCE_COEFFICIENT)
        }
    }

    fun getCurrent() : Double
    {
        val margain = currentMaxOffset * currentCoefficient * Random().nextDouble()
        val current = currentValue + (margain * if (Random().nextBoolean()) 1.0 else -1.0)
        return Math.min(Math.max(current, MIN_VALUE), MAX_VALUE)
    }
}