package com.sentibytes.util

import java.util.*

class Util
{
    companion object
    {
        //TODO expand to collections
        fun <T> randomElement(collection: Collection<T>) : T
        {
            if (collection.isEmpty())
            {
                throw Exception("Random element selection requires a collection with at least one element.")
            }

            return collection.elementAt(Math.floor(Random().nextDouble() * collection.size).toInt())
        }

        fun <T> categoryRoll(categories: Map<T, Double>) : T?
        {
            if (categories.isEmpty())
            {
                return null
            }

            val categoryList = categories.toList()

            var currentSum = 0.0
            val random = Random().nextDouble() * categoryList.sumByDouble { it.second }
            categoryList.forEach{
                currentSum += it.second
                if (random < currentSum || categoryList.indexOf(it) == categoryList.size - 1)
                {
                    return it.first
                }
            }

            throw RollException("random draw fell outside sumtotal")
        }

        fun getValueFromRange(lower: Double, upper: Double, coefficient: Double) : Double
        {
            val delta = upper - lower
            return lower + (coefficient * delta)
        }
    }
}