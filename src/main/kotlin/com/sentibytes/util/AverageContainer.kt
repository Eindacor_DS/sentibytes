package com.sentibytes.util

import com.mongodb.BasicDBObjectBuilder
import com.mongodb.DBObject
import com.sentibytes.infrastructure.DocumentBuilder
import org.bson.Document

class AverageContainer(average : Double = 0.0, private var count : Int = 0)
{
    var average: Double = average
        private set

    companion object
    {
        val AVERAGE_STRING = "average"
        val COUNT_STRING = "ALL"

        fun getBlankDBDoc() : DBObject
        {
            return BasicDBObjectBuilder()
                    .add(AVERAGE_STRING, 0.0)
                    .add(COUNT_STRING, 0)
                    .get()
        }
    }

    constructor(document: Document) : this(document.getDouble(AVERAGE_STRING), document.getInteger(COUNT_STRING))

    fun addAverage(averageToAdd: Double, countToAdd: Int) : Double
    {
        average = combineAverages(average, count, averageToAdd, countToAdd)
        count += countToAdd
        return average
    }

    fun addValue(value: Double) : Double{
        average = combineAverages(average, count, value, 1)
        count++
        return average
    }

    fun toDoc() : Document
    {
        return DocumentBuilder(AVERAGE_STRING, average)
                .put(COUNT_STRING, count)
                .get()
    }

    private fun combineAverages(initialAvg: Double, initialCount: Int, newAvg: Double, newCount: Int) : Double
    {
        val updatedCount = initialCount + newCount
        val updatedAvg = (initialAvg * initialCount) + (newAvg * newCount)
        return updatedAvg/updatedCount
    }
}