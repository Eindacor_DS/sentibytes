package com.sentibytes.util

import com.mongodb.BasicDBList
import com.mongodb.BasicDBObjectBuilder
import com.mongodb.DBObject
import com.sentibytes.knowledge.KnowledgeBase
import com.sentibytes.attributes.AttributeType
import com.sentibytes.attributes.SentibyteAttribute
import com.sentibytes.community.CommunityCache
import com.sentibytes.community.CommunityKey
import com.sentibytes.infrastructure.Collection
import com.sentibytes.infrastructure.DBKey
import com.sentibytes.infrastructure.DataAccess
import com.sentibytes.infrastructure.DocumentBuilder
import com.sentibytes.knowledge.KnowledgeKey
import com.sentibytes.metadata.SentibyteMetadata
import org.bson.Document
import org.bson.types.ObjectId
import java.util.Random
import java.util.UUID

class MockDBUtil {

    companion object {
        private val DB_POPULATORS : MutableMap<Collection, () -> Unit> = HashMap()

        private val MONGO_DATABASE = DataAccess.getDatabase()

        private var interPersonalAttributeCount : Int = 0;

        init
        {
            DB_POPULATORS[Collection.ATTRIBUTES] = {
                DataAccess.fromCollection(Collection.ATTRIBUTES).insertMany(getAttributes())
            }

            DB_POPULATORS[Collection.SENTIBYTES] = {
                DataAccess.fromCollection(Collection.COMMUNITIES).find(Document()).forEach{
                    val community = CommunityCache.get(it.getString(DBKey.ID.key))
                    for (i in 0 until 1)
                    {
                        var depth = 1 + (Random().nextInt() % 3)
                        createSentibyteWithParents(depth, community.communityId)
                    }
                }
            }

            DB_POPULATORS[Collection.SENTIBYTE_METADATA] = {
                DataAccess.fromCollection(Collection.SENTIBYTES).find().forEach{
                    DataAccess.fromCollection(Collection.SENTIBYTE_METADATA).insertOne(SentibyteMetadata.getBlank(it!!.getString(DBKey.ID.key)))
                }
            }

            DB_POPULATORS[Collection.COMMUNITIES] = {
                DataAccess.fromCollection(Collection.COMMUNITIES).insertOne(
                        DocumentBuilder.id(ObjectId().toString()).put(CommunityKey.MAX_CHILDREN.name, 10)
                                .put(CommunityKey.CHILD_AGE.name, 100)
                                .put(CommunityKey.COMMUNICATIONS_PER_CYCLE.name, 20)
                                .get()
                )
            }

            DB_POPULATORS[Collection.KNOWLEDGE] = {
                for (i in 0..KnowledgeBase.TRUTH_SIZE)
                {
                    DataAccess.fromCollection(Collection.KNOWLEDGE).insertOne(
                            DocumentBuilder.id(ObjectId().toString()).put(KnowledgeKey.VALUE.name, UUID.randomUUID().toString()).get()
                    )
                }
            }
        }

        fun clearAllDbs()
        {
            Collection.values().forEach{
                clearDB(it)
            }
        }

        fun clearDB(collection: Collection)
        {
            DataAccess.fromCollection(collection).deleteMany(Document())
        }

        fun populate(collection: Collection)
        {
            DB_POPULATORS[collection]!!.invoke()
        }

        private fun getFalseKnowledgeStub() : DBObject
        {
            return BasicDBObjectBuilder()
                    .add(Random().nextInt(10).toString(), UUID.randomUUID().toString())
                    .add(Random().nextInt(10).toString(), UUID.randomUUID().toString())
                    .add(Random().nextInt(10).toString(), UUID.randomUUID().toString())
                    .add(Random().nextInt(10).toString(), UUID.randomUUID().toString())
                    .get()
        }

        private fun createSentibyteWithParents(depth: Int, communityId: String): String
        {
            return if (depth == 0)
            {
                CommunityCache.get(communityId).createSentibyte()
            } else
            {
                val motherId = createSentibyteWithParents(depth - 1, communityId)
                val fatherId = createSentibyteWithParents(depth - 1, communityId)
                CommunityCache.get(communityId).createSentibyte(motherId, fatherId)
            }
        }

        private fun getAccurateKnowledgeStub() : BasicDBList
        {
            val knowledge = BasicDBList()
            for (i in 0 until 30)
            {
                knowledge.add(Random().nextInt(40))
            }

            return knowledge
        }

        private fun getAttributes() : List<Document>
        {
            val attributes = mutableListOf<Document>()

            SentibyteAttribute.values().forEach {
                val attribute = DocumentBuilder.id(ObjectId().toString()).put("type", it.type.name)
                        .put("name", it.name)
                        .put("min", it.min)
                        .put("max", it.max)
                        .get()
                attributes.add(attribute)
            }

            return attributes
        }

        private fun getInterpersonalAttributeCount() : Int
        {
            if (interPersonalAttributeCount == 0) {
                SentibyteAttribute.values().forEach {
                    if (it.type.equals(AttributeType.INTERPERSONAL)) {
                        interPersonalAttributeCount++
                    }
                }
            }

            return interPersonalAttributeCount
        }

        private fun getRandomAttributePriorities() : List<Int>
        {
            val priorityList = mutableListOf(getInterpersonalAttributeCount())

            for (i in 0 until getInterpersonalAttributeCount())
            {
                priorityList.add(i + 1)
            }

            priorityList.shuffle()
            return priorityList
        }
    }
}