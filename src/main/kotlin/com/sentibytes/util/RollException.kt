package com.sentibytes.util

class RollException(message: String) : Exception(message)
{
}