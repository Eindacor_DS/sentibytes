package com.sentibytes

import com.mongodb.BasicDBList
import com.sentibytes.attributes.AttributeDesire
import com.sentibytes.attributes.AttributeProperty
import com.sentibytes.attributes.AttributeType
import com.sentibytes.attributes.SentibyteAttribute
import com.sentibytes.cache.SentibyteCache
import com.sentibytes.cache.SessionCache
import com.sentibytes.communication.*
import com.sentibytes.community.AvailabilityType
import com.sentibytes.community.Community
import com.sentibytes.community.CommunityCache
import com.sentibytes.infrastructure.*
import com.sentibytes.infrastructure.Collection
import com.sentibytes.knowledge.KnowledgeBase
import com.sentibytes.metadata.SentibyteMetadata
import com.sentibytes.metadata.SentibyteMetadataCategory
import com.sentibytes.perceptions.Perception
import com.sentibytes.perceptions.PerceptionProperty
import com.sentibytes.session.*
import com.sentibytes.util.AverageContainer
import com.sentibytes.util.FluctuatingValue
import com.sentibytes.util.Util
import org.bson.Document
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.collections.HashSet

class Sentibyte(document: Document) : DocumentBase(document, Collection.SENTIBYTES)
{
    companion object
    {
        //community attributes
        const val FRIEND_COUNT = 12
        const val BOND_COUNT = 6
        
        fun getAncestors(id: String, depth: Int? = null) : Map<String, Int>
        {
            val ancestors : MutableMap<String, Int> = HashMap()
            addAncestors(id, 0, ancestors, depth)
            return ancestors
        }

        fun getAncestorList(id: String, depth: Int? = null) : List<String>
        {
            return getAncestors(id, depth).map { it.key }.toList()
        }

        /**
         *
         */
        private fun addAncestors(id: String, tier: Int, ancestors : MutableMap<String, Int> = HashMap(), depth: Int? = null)
        {
            // sentibytes already in the map have already had their ancestry added
            getParents(id).filter{
                it !in ancestors
            }.forEach{
                ancestors.put(it, tier)
                if (depth == null || tier < depth)
                {
                    addAncestors(it,tier + 1, ancestors, depth)
                }
            }
        }

        fun getParents(id: String) : List<String>
        {
            val selfArray = BasicDBList()
            selfArray.add(id)
            return DataAccess.fromCollection(Collection.SENTIBYTES).find(
                    DocumentBuilder(SentibyteProperty.CHILDREN.name, id).get()
            ).map(Sentibyte.Companion::documentToId).toList()
        }

        fun getSiblings(id: String) : List<String>
        {
            val parents = getParents(id)
            return DataAccess.fromCollection(Collection.SENTIBYTES).find(
                    DocumentBuilder(SentibyteProperty.PARENTS.name, DocumentBuilder.iN(parents)).get()
            ).map(Sentibyte.Companion::documentToId).toList()
        }

        private fun documentToId(doc: Document) : String
        {
            return doc.getString(DBKey.ID.key)
        }

        fun getAvailability(id: String) : Pair<AvailabilityType, String?>
        {
            val sessionId = SentibyteCache.get().get(id)?.currentSession ?: DataAccess.get(id, Collection.SENTIBYTES)!!.getString(SentibyteProperty.CURRENT_SESSION.name)
            val availability : AvailabilityType

            if (sessionId == null)
            {
                availability = AvailabilityType.ALONE
            }
            else
            {
                val session = SessionCache.get().getOrPut(sessionId)

                availability =  if (session.participants.size >= session.sessionType.maxParticipants)
                {
                    AvailabilityType.IN_FULL_SESSION
                }
                else
                {
                    AvailabilityType.IN_SESSION
                }
            }

            return Pair(availability, sessionId)
        }

        fun getCycle(id: String) : Cycle
        {
            return if (DataAccess.get(id, Collection.SENTIBYTES)!![SentibyteProperty.CURRENT_SESSION.name] == null)
            {
                AloneCycle()
            }
            else
            {
                SessionCycle()
            }
        }
    }

    val sentibyteId = documentId

    // key = other documentId
    private val currentInteractions : MutableMap<String, Interaction> = HashMap()

    enum class BondResponse {NO, NOT_NOW, YES}

    private val bondCycleMinmum : Int = 20

    private val name : String = getValue(SentibyteProperty.NAME.name)!!

    var communityId : String? = getValue(SentibyteProperty.COMMUNITY_ID.name)
        private set

    var age : Int = getValue(SentibyteProperty.AGE.name)!!
        private set

    val parents : MutableSet<String> = getSet(SentibyteProperty.PARENTS.name)

    private val children : MutableSet<String> = getSet(SentibyteProperty.CHILDREN.name)

    private var friends : MutableSet<String> = getSet(SentibyteProperty.FRIENDS.name)

    // TODO useless key converter
    var bonds : MutableMap<String, Int> = getHashMap(SentibyteProperty.BONDS.name, { key -> key }, { value -> value as Int})
        private set

    val contacts : MutableSet<String> = getSet(SentibyteProperty.CONTACTS.name)

    // MutableMap<SentibyteAttribute, FluctuatingValue>
    val attributes : Map<SentibyteAttribute, FluctuatingValue> = getHashMap(SentibyteProperty.ATTRIBUTES.name,
            {key -> SentibyteAttribute.getValueFromIdString(key)!!},
            {value ->
                val valueDoc = value as Document
                FluctuatingValue(
                        valueDoc.getDouble(AttributeProperty.LOWER_BOUND.name),
                        valueDoc.getDouble(AttributeProperty.UPPER_BOUND.name),
                        valueDoc.getDouble(AttributeProperty.BASE_COEFFICIENT.name),
                        valueDoc.getDouble(AttributeProperty.FLUCTUATION.name),
                        valueDoc.getDouble(AttributeProperty.SENSITIVITY.name),
                        valueDoc.getDouble(AttributeProperty.CURRENT_VALUE.name))
            })

    val desiredAttributes: Map<SentibyteAttribute, AttributeDesire> = getHashMap(SentibyteProperty.DESIRED_ATTRIBUTES.name,
            {key -> SentibyteAttribute.getValueFromIdString(key)!!},
            {value ->
                val valueDoc = value as Document
                AttributeDesire(valueDoc.getInteger("priority"), valueDoc.getDouble("value"))
            })

    private var cyclesInCurrentSession: Int = 0

    private val deathCoefficient : Double = getValue(SentibyteProperty.DEATH_COEFFICIENT.name)!!

    var socialCooldown : Int = getValue(SentibyteProperty.SOCIAL_COOLDOWN.name)!!
        set

    private var metadata : SentibyteMetadata? = null

    // key = other documentId
    private val perceptionCache : MutableMap<String, Perception> = HashMap()

    // key = KNOWLEDGE index
    // TODO useless key converter
    private val falseKnowledge : MutableMap<String, String> = getMap(SentibyteProperty.FALSE_KNOWLEDGE.name, { key -> key }, { value -> value.toString()})

    private val accurateKnowledge : MutableSet<String> = getSet(SentibyteProperty.ACCURATE_KNOWLEDGE.name)

    var currentSession: String? = getValue(SentibyteProperty.CURRENT_SESSION.name)

    constructor(id: String) : this(DataAccess.get(id, Collection.SENTIBYTES)!!)

    override fun save()
    {
        currentInteractions.forEach{
            it.value.updateGuesses(cyclesInCurrentSession, CommunityCache.get(communityId!!).communicationsPerCycle)
            val perception = getPerception(it.value.otherId)
            perception.addInteraction(it.value, InteractionType.IN_PERSON)
            perception.save()
            updateContacts()
            it.value.save()
        }

        perceptionCache.forEach{
            it.value.save()
        }

        metadata?.save()

        writeToDB()
    }

    override fun pack(): Document
    {
        return DocumentBuilder()
                .put(SentibyteProperty.PARENTS.name, parents)
                .put(SentibyteProperty.AGE.name, age)
                .put(SentibyteProperty.CHILDREN.name, children)
                .put(SentibyteProperty.FRIENDS.name, friends)
                .put(SentibyteProperty.BONDS.name, bonds)
                .put(SentibyteProperty.CURRENT_SESSION.name, currentSession)
                .put(SentibyteProperty.ACCURATE_KNOWLEDGE.name, accurateKnowledge.toList())
                .put(SentibyteProperty.FALSE_KNOWLEDGE.name, falseKnowledge.map{
                    Pair(it.key, it.value)
                }.toMap())
                .put(SentibyteProperty.ATTRIBUTES.name, packAttributes())
                .put(SentibyteProperty.COMMUNITY_ID.name, communityId)
                .put(SentibyteProperty.SOCIAL_COOLDOWN.name, socialCooldown)
                .get()
    }

    private fun packAttributes() : Document
    {
        val attributesDoc = DocumentBuilder()
        attributes.forEach{
            attributesDoc.put(SentibyteAttribute.getDBId(it.key),
                    DocumentBuilder(AttributeProperty.CURRENT_VALUE.name, it.value.getCurrent())
                            .put(AttributeProperty.LOWER_BOUND.name, it.value.lowerBound)
                            .put(AttributeProperty.UPPER_BOUND.name, it.value.upperBound)
                            .put(AttributeProperty.BASE_COEFFICIENT.name, it.value.baseCoefficient)
                            .put(AttributeProperty.FLUCTUATION.name, it.value.fluctuationCoefficient)
                            .put(AttributeProperty.SENSITIVITY.name, it.value.sensitivity)
                            .get())
        }
        return attributesDoc.get()
    }

    fun reflect()
    {
        val query = DocumentBuilder(InteractionKey.OWNER_ID.name, documentId).get()
        val memoryOtherIds = DataAccess.fromCollection(Collection.INTERACTIONS)
                .find(query)
                .distinctBy{document -> document.getString(InteractionKey.OTHER_ID.name) }
                .map{document -> document.getString(InteractionKey.OTHER_ID.name)}
                .toList()

        if (memoryOtherIds.isNotEmpty())
        {
            val randomOtherId = Util.randomElement(memoryOtherIds)!!
//            if (randomOtherId !in community.getMembers())
//            {
//                TODO("add behavior for mourning/missing")
//            }

            val interactions = Interaction.getInteractions(documentId, randomOtherId)

            if (interactions.isEmpty())
            {
                return
            }

            val randomInteraction = Util.randomElement(interactions)

            getPerception(randomOtherId).addInteraction(randomInteraction, InteractionType.MEMORY)
            modified = true
            updateContacts()
        }
    }

    fun learn()
    {
        var learned : Pair<String, String>?
        learned = if (proc(SentibyteAttribute.INQUISITIVE))
        {
            val known = accurateKnowledge.toMutableSet()
            known.addAll(falseKnowledge.map { pair -> pair.key })
            val unknownOrRandom = KnowledgeBase.getRandomUnknown(known)?: KnowledgeBase.getRandomTruth()
            unknownOrRandom
        }
        else
        {
            KnowledgeBase.getRandomTruth()
        }

        if (!proc(SentibyteAttribute.INTELLIGENCE))
        {
            accurateKnowledge.remove(learned.first)

            if (falseKnowledge.keys.contains(learned.first))
            {
                learned = Pair(learned.first, obscureKnowledge(falseKnowledge[learned.first]!!))
            }
            else
            {
                learned = Pair(learned.first, obscureKnowledge(learned.second))
            }

            falseKnowledge[learned.first] = learned.second
        }
        else
        {
            falseKnowledge.remove(learned.first)
            accurateKnowledge.add(learned.first)
        }

        incrementMetadata(SentibyteMetadataCategory.LEARNED_ON_OWN)
    }

    private fun fluctuateTraits()
    {
        attributes.forEach{
            it.value.fluctuate()
        }
    }

    fun attemptBond(otherId: String) : Boolean
    {
        val other : Sentibyte = SentibyteCache.get().getOrPut(otherId)
        val otherResponse = other.wantsToBond(documentId)

        var childMade = false
        var instanceValue : Double?
        var toIncrement : SentibyteMetadataCategory?
        when (otherResponse){
            BondResponse.NO ->
            {
                toIncrement = SentibyteMetadataCategory.BONDS_DENIED
                instanceValue = -.1
            }
            BondResponse.NOT_NOW ->
            {
                toIncrement = SentibyteMetadataCategory.BONDS_POSTPONED
                instanceValue = .1
            }
            else ->
            {
                toIncrement = SentibyteMetadataCategory.BONDS_CONFIRMED
                instanceValue = .1

                if (other.proc(SentibyteAttribute.CONCUPISCENT))
                {
                    CommunityCache.get(communityId!!).createSentibyte(documentId, other.documentId)
                    childMade = true
                }
            }
        }

        incrementMetadata(toIncrement)
        getPerception(otherId).addInstance(instanceValue)
        updateContacts()

        if (other.currentSession == null)
        {
            SentibyteCache.get().unload(otherId)
        }

        return childMade
    }

    fun procHealth() : Boolean
    {
        return if (Random().nextDouble() < deathCoefficient)
        {
            val community = CommunityCache.get(communityId!!)
            communityId = null
            save()
            community.updateMembers()
            false
        }
        else
        {
            true
        }
    }

    fun update()
    {
        contacts.filter{
            val otherId = it
            val communityMembers = DataAccess.fromCollection(Collection.SENTIBYTES).find(DocumentBuilder(SentibyteProperty.COMMUNITY_ID.name, communityId!!).get()).map{
                doc -> doc.getString(DBKey.ID.key)
            }.toSet()
            otherId !in communityMembers
        }.forEach{
            contacts.remove(it)
            bonds.remove(it)
            //TODO identify behavior for lost family members, missing contacts
        }

        updateContacts()

        age++

        if (currentSession != null)
        {
            cyclesInCurrentSession++
            incrementMetadata(SentibyteMetadataCategory.CYCLES_IN_SESSION)
        }
        else
        {
            incrementMetadata(SentibyteMetadataCategory.CYCLES_ALONE)
        }

        if (proc(SentibyteAttribute.VOLATILITY))
        {
            fluctuateTraits()
        }
    }

    //TODO ensure this is run after all cached S' are saved, to allow all targets can be established via DB queries
    fun sendInvitation() : Boolean
    {
        updateContacts()
        val invitationTargets = getInvitationTargets()
        val contactType = invitationTargets.first
        val targets = invitationTargets.second

        return if (targets.isEmpty())
        {
            logConnection(false, contactType)
            false
        }
        else
        {
            connectWithTarget(targets, contactType)
        }
    }

    private fun connectWithTarget(targets: List<String>, contactType: ContactType) : Boolean
    {
        val targetMap : MutableMap<String, Double> = HashMap()
        targets.forEach{
            targetMap[it] = getPerception(it).getRating()
        }

        var selectedId: String = Util.categoryRoll(targetMap)!!

        // don't proc wantsToConnect for inviter, since they're already picking their favorite via categoryRoll and
        // already proc'ed sociable

        while (!otherWantsToConnect(selectedId))
        {
            targetMap.remove(selectedId)

            if (targetMap.isEmpty())
            {
                logConnection(false, contactType)
                return false
            }

            selectedId = Util.categoryRoll(targetMap)!!
        }

        connect(selectedId)
        logConnection(true, contactType)
        return true
    }

    private fun otherWantsToConnect(otherId: String) : Boolean
    {
        val otherDoc = DataAccess.get(otherId, Collection.SENTIBYTES)!!
        val otherAttributes = otherDoc[SentibyteProperty.ATTRIBUTES.name] as Document
        val regardDoc = otherAttributes[SentibyteAttribute.getDBId(SentibyteAttribute.REGARD)] as Document
        val otherCurrentRegard = (regardDoc).getDouble(AttributeProperty.CURRENT_VALUE.name)!!
        var threshold = 1.0 - otherCurrentRegard

        //TODO remove
        threshold = 0.0

        val otherPerception = DataAccess.fromCollection(Collection.PERCEPTIONS).find(
                DocumentBuilder()
                        .put(PerceptionProperty.OTHER_ID.name, sentibyteId)
                        .put(PerceptionProperty.OWNER_ID.name, otherId)
                        .get()
        ).first()

        val rating = if (otherPerception == null)
        {
            val lower = regardDoc.getDouble(AttributeProperty.LOWER_BOUND.name)!!
            val upper = regardDoc.getDouble(AttributeProperty.UPPER_BOUND.name)!!
            val baseCoefficient = regardDoc.getDouble(AttributeProperty.BASE_COEFFICIENT.name)!!
            Util.getValueFromRange(lower, upper, baseCoefficient)
        }
        else
        {
            (otherPerception.get(PerceptionProperty.OVERALL_RATING.name) as Document).getDouble(AverageContainer.AVERAGE_STRING)!!
        }

        return rating >= threshold
    }

    private fun getInvitationTargets(): Pair<ContactType, List<String>>
    {
        val contactType : ContactType
        var targets: List<String>

        if (contacts.isEmpty() || proc(SentibyteAttribute.ADVENTEROUS))
        {
            targets = getAvailableStrangers()

            contactType = ContactType.STRANGERS
            incrementMetadata(SentibyteMetadataCategory.INVITATIONS_TO_STRANGERS)
        }
        else if (proc(SentibyteAttribute.PICKINESS) && friends.isNotEmpty())
        {
            targets = friends.filter {
                DataAccess.get(it, Collection.SENTIBYTES)!!.getInteger(SentibyteProperty.SOCIAL_COOLDOWN.name) > 0
            }

            contactType = ContactType.FRIENDS
            incrementMetadata(SentibyteMetadataCategory.INVITATIONS_TO_FRIENDS  )
        }
        else
        {
            targets = contacts.filter {
                DataAccess.get(it, Collection.SENTIBYTES)!!.getInteger(SentibyteProperty.SOCIAL_COOLDOWN.name) > 0
            }

            contactType = ContactType.CONTACTS
            incrementMetadata(SentibyteMetadataCategory.INVITATIONS_TO_CONTACTS)
        }

        return Pair(contactType, targets)
    }

    /**
     * Filters out connections that still have a cooldown, that
     */
    private fun getAvailableStrangers(): List<String>
    {
        val query = DocumentBuilder.ne(sentibyteId)
                // S' could have memories of S' they haven't met yet via gossip. Strangers are only S' that are not
                // in the contact list
                .putNin(contacts.toList())
                .wrapId()
                .put(SentibyteProperty.SOCIAL_COOLDOWN.name, DocumentBuilder.gt(0))
                .get()

        return DataAccess.fromCollection(Collection.SENTIBYTES).find(query).map{doc -> doc.getString(DBKey.ID.key)}.toList()
    }

    private fun logConnection(wasSuccessful: Boolean, contactType: ContactType)
    {
        val toIncrement: SentibyteMetadataCategory = when (contactType)
        {
            ContactType.STRANGERS -> {
                if (wasSuccessful) SentibyteMetadataCategory.SUCCESSFUL_CONNECTIONS_STRANGERS else SentibyteMetadataCategory.UNSUCCESSFUL_CONNECTIONS_STRANGERS
            }
            ContactType.CONTACTS -> {
                if (wasSuccessful) SentibyteMetadataCategory.SUCCESSFUL_CONNECTIONS_CONTACTS else SentibyteMetadataCategory.UNSUCCESSFUL_CONNECTIONS_CONTACTS
            }
            ContactType.FRIENDS -> {
                if (wasSuccessful) SentibyteMetadataCategory.SUCCESSFUL_CONNECTIONS_FRIENDS else SentibyteMetadataCategory.UNSUCCESSFUL_CONNECTIONS_FRIENDS
            }
        }

        incrementMetadata(toIncrement)
    }

    private fun connect(otherId: String)
    {
        val status = getAvailability().first
        val availability = getAvailability(otherId)

        when(status)
        {
            AvailabilityType.ALONE -> {
                when (availability.first) {
                    AvailabilityType.ALONE -> {
                        //dynamically set session type based on sociable
                        createNewSession(otherId, SessionType.LARGE)
                    }
                    AvailabilityType.IN_SESSION -> {
                        goToSession(availability.second!!, otherId)
                    }
                    AvailabilityType.IN_FULL_SESSION -> {
                        //dynamically set session type based on sociable
                        createNewSession(otherId, SessionType.LARGE)
                    }
                }
            }
            AvailabilityType.IN_SESSION -> {
                when (availability.first) {
                    AvailabilityType.ALONE -> {
                        SentibyteCache.get().getOrPut(otherId).goToSession(currentSession!!, sentibyteId)
                    }
                    AvailabilityType.IN_SESSION -> {
                        if (Random().nextBoolean())
                        {
                            goToSession(availability.second!!, sentibyteId)
                        }
                        else
                        {
                            SentibyteCache.get().getOrPut(otherId).goToSession(currentSession!!, otherId)
                        }
                    }
                    AvailabilityType.IN_FULL_SESSION -> {
                        SentibyteCache.get().getOrPut(otherId).goToSession(currentSession!!, sentibyteId)
                    }
                }
            }
            AvailabilityType.IN_FULL_SESSION -> {
                when (availability.first) {
                    AvailabilityType.ALONE -> {
                        createNewSession(otherId, SessionType.LARGE)
                    }
                    AvailabilityType.IN_SESSION -> {
                        goToSession(availability.second!!, otherId)
                    }
                    AvailabilityType.IN_FULL_SESSION -> {
                        createNewSession(otherId, SessionType.LARGE)
                    }
                }
            }
        }
    }

    private fun goToSession(sessionId: String, inviter: String? = null)
    {
        //if in current session, leave
        if (currentSession != null)
        {
            SessionCache.get().getOrPut(currentSession!!).leaveSession(sentibyteId)
        }

        SessionCache.get().getOrPut(sessionId).addParticipant(sentibyteId, inviter)
    }

    private fun createNewSession(otherId: String, sessionType: SessionType)
    {
        val sessionId = SessionCache.newSession(sentibyteId, otherId, sessionType)
        goToSession(sessionId)
        SentibyteCache.get().getOrPut(otherId).goToSession(sessionId)
    }

    fun getAvailability() : Pair<AvailabilityType, String?>
    {
        if (currentSession == null)
        {
            return Pair(AvailabilityType.ALONE, currentSession)
        }

        val session = SessionCache.get().getOrPut(currentSession!!)

        return if (session.participants.size >= session.sessionType.maxParticipants)
        {
            Pair(AvailabilityType.IN_FULL_SESSION, currentSession)
        }
        else
        {
            Pair(AvailabilityType.IN_SESSION, currentSession)
        }
    }

    private fun obscureKnowledge(string: String) : String
    {
        val randomIndex = Random().nextInt(string.length)

        val newString = StringBuilder()
        for (i in 0 until string.length)
        {
            if (i == randomIndex)
            {
                var randomChar: Char
                do
                {
                    randomChar = '!'.plus(Random().nextInt(('!'..'z').count()))
                } while (randomChar == string[randomIndex])

                newString.append(randomChar)
            }
            else
            {
                newString.append(string[i])
            }
        }

        return newString.toString()
    }

    private fun influence(sentibyteAttribute: SentibyteAttribute, value: Double, coefficient: Double)
    {
        attributes[sentibyteAttribute]!!.influence(value, coefficient)
    }

    private fun receiveTransmission(transmission: Transmission)
    {
        if (proc(SentibyteAttribute.TRUSTING))
        {
            if (transmission.knowledge != null && proc(SentibyteAttribute.INTELLECTUAL))
            {
                transmission.knowledge.forEach {
                    if (falseKnowledge.containsKey(it))
                    {
                        falseKnowledge.remove(it)
                        incrementMetadata(SentibyteMetadataCategory.CORRECTED_BY_OTHERS)
                    }
                }

                accurateKnowledge.addAll(transmission.knowledge)
                incrementMetadata(SentibyteMetadataCategory.LEARNED_FROM_OTHERS, transmission.knowledge.size)
            }

            if (transmission.falseKnowledge != null)
            {
                if (!proc(SentibyteAttribute.INTELLIGENCE))
                {
                    accurateKnowledge.removeAll(transmission.falseKnowledge.keys)
                    falseKnowledge.putAll(transmission.falseKnowledge)
                    incrementMetadata(SentibyteMetadataCategory.MISLED_BY_OTHERS, falseKnowledge.size)
                }
            }
        }

        if (transmission.gossip != null)
        {
            val otherId = transmission.gossip.otherId
            perceptionCache.getOrPut(otherId){
                Perception.getPerception(documentId, otherId)
            }.addInteraction(transmission.gossip, InteractionType.RUMOR)
            updateContacts()
        }

        if (transmission.brag != null)
        {
            // TODO("brag system")
        }
    }

    /**
     * When a Sentibyte broadcasts, all others are exposed to that transmission, as if they were absorbing other Sentibytes'
     * behaviors without directly interacting with them.
     */
    fun exposeToTransmission(transmission: Transmission)
    {
        val sourceId = transmission.sourceId

        if (transmission.transmissionType == TransmissionType.STATEMENT || proc(SentibyteAttribute.OBSERVANT))
        {
            contacts.add(sourceId)
            getInteraction(sourceId).addTransmission(transmission)

            if (transmission.targets.contains(documentId))
            {
                receiveTransmission(transmission)
            }

            modified=true
            // TODO potential to identify if gossip was about self, similar to overhearing someone talk about you
        }
    }

    private fun getInteraction(otherId: String) : Interaction
    {
        return currentInteractions.getOrPut(otherId){
            Interaction.newInteraction(sentibyteId, otherId)
        }
    }

    fun broadcast(availableTargets: List<String>) : Transmission
    {
        var transmissionAccurateKnowledge : MutableSet<String>? = null
        var transmissionFalseKnowledge : MutableMap<String, String>? = null

        val transmissionType = if (proc(SentibyteAttribute.TALKATIVE)) TransmissionType.STATEMENT else TransmissionType.SIGNAL

        if (transmissionType.equals(TransmissionType.STATEMENT))
        {
            val allKnowledge = accurateKnowledge.toList() + falseKnowledge.map { pair -> pair.key }.toList()
            if (allKnowledge.isNotEmpty() && proc(SentibyteAttribute.INTELLECTUAL))
            {
                val randomIndex = Math.floor(Random().nextDouble() * allKnowledge.size).toInt()
                val knowledgeKey = allKnowledge.get(randomIndex)

                if (accurateKnowledge.contains(knowledgeKey))
                {
                    transmissionAccurateKnowledge = HashSet()
                    transmissionAccurateKnowledge.add(knowledgeKey)
                }
                else
                {
                    transmissionFalseKnowledge = HashMap()
                    transmissionFalseKnowledge.put(knowledgeKey, falseKnowledge[knowledgeKey]!!)
                }
            }
        }

        val selectedTargets: List<String> = getSelectedTargets(availableTargets)

        return Transmission(
                documentId,
                selectedTargets,
                attributes[SentibyteAttribute.POSITIVITY]!!.getCurrent(),
                attributes[SentibyteAttribute.ENERGY]!!.getCurrent(),
                transmissionType,
                transmissionAccurateKnowledge,
                transmissionFalseKnowledge,
                getGossip(selectedTargets),
                getBrag()
        )
    }

    private fun getSelectedTargets(targets: List<String>): List<String>
    {
        if (targets.isEmpty())
        {
            return ArrayList()
        }

        //TODO currently this procs sociable and wants to connect, possibly too restrictive for communication
        val selectedTargets: MutableSet<String> = HashSet()
        do
        {
            val randomUnselected = Util.randomElement(targets.filter { it -> !selectedTargets.contains(it) })
            selectedTargets.add(randomUnselected)
        } while (selectedTargets.size < targets.size && proc(SentibyteAttribute.SOCIABLE))

        return selectedTargets.toList()
    }

    private fun getBrag() : Pair<SentibyteAttribute, Double>?
    {
        if (proc(SentibyteAttribute.CONFIDENT))
        {
            val interpersonalAttributes = attributes.filter{
                it.key.type == AttributeType.INTERPERSONAL
            }.map{pair -> pair.key}

            val randomIndex = Math.floor(Random().nextDouble() * interpersonalAttributes.size).toInt()
            val randomAttribute = interpersonalAttributes[randomIndex]
            return Pair(randomAttribute, attributes[randomAttribute]!!.getCurrent())
        }

        return null
    }

    private fun getGossip(targets: List<String>) : Interaction?
    {
        if (proc(SentibyteAttribute.GOSSIPY))
        {
            val query = DocumentBuilder(InteractionKey.OWNER_ID.name, documentId).put(InteractionKey.OTHER_ID.name, DocumentBuilder.nin(targets)).get()
            val gossipTargets = DataAccess.fromCollection(Collection.INTERACTIONS)
                    .find(query)
                    .toList()

            if (gossipTargets.isNotEmpty())
            {
                val randomIndex = Math.floor(Random().nextDouble() * gossipTargets.size).toInt()
                return Interaction(gossipTargets[randomIndex])
            }
        }

        return null
    }

    fun tickCooldown()
    {
        socialCooldown--
    }


    // TODO make bond desire restricted by current children and children desired for each sentibyte
    fun wantsToBond(otherId: String) : BondResponse
    {
        if (otherId !in bonds)
        {
            return BondResponse.NO
        }
        else
        {
            // TODO also reject if in current session
            return if (currentSession != null)
            {
                BondResponse.NOT_NOW
            }
            else
            {
                if (getPerception(otherId).getRating() < .5) BondResponse.NOT_NOW else BondResponse.YES
            }
        }
    }

    fun proc(sentibyteAttribute: SentibyteAttribute) : Boolean
    {
        return Random().nextDouble() < attributes[sentibyteAttribute]!!.getCurrent()
    }

    fun getCurrent(sentibyteAttribute: SentibyteAttribute) : Double
    {
        return attributes.get(sentibyteAttribute)!!.getCurrent()
    }

    private fun wantsToConnect(otherId: String) : Boolean
    {
        // TODO take into account current availability, whether they want to leave their current session
        // TODO calc average rating of S' in current session and compare to other
        val minimumRating = 1.0 - attributes[SentibyteAttribute.TOLERANCE]!!.getCurrent()
        val accepted = getPerception(otherId).getRating() >= minimumRating

        if (accepted)
        {
            incrementMetadata(SentibyteMetadataCategory.ACCEPTED_COUNT)
        }
        else
        {
            incrementMetadata(SentibyteMetadataCategory.REJECTION_COUNT)
        }

        return accepted
    }

    fun incrementMetadata(sentibyteMetadataCategory: SentibyteMetadataCategory)
    {
        initializeMetadata()
        metadata!!.increment(sentibyteMetadataCategory)
    }

    fun incrementMetadata(sentibyteMetadataCategory: SentibyteMetadataCategory, count : Int)
    {
        initializeMetadata()
        metadata!!.increment(sentibyteMetadataCategory, count)
    }

    private fun initializeMetadata()
    {
        if (metadata == null)
        {
            val query = DocumentBuilder("SENTIBYTE_ID", sentibyteId).get()
            metadata = SentibyteMetadata(DataAccess.fromCollection(Collection.SENTIBYTE_METADATA).find(query).first()!!)
        }
    }

    private fun getBondCandidates() : List<String>
    {
        val bondThreshold = attributes[SentibyteAttribute.PICKINESS]!!.getCurrent()
        val ancestors = Sentibyte.getAncestors(sentibyteId, 3)
        val query = DocumentBuilder.nin(ancestors.map{it.key}.toList())
                .wrap(PerceptionProperty.OTHER_ID.name)
                .put(PerceptionProperty.OWNER_ID.name, documentId)
                .put(PerceptionProperty.CYCLES_OBSERVED.name, DocumentBuilder.gte(bondCycleMinmum).get())
                .put(PerceptionProperty.OVERALL_RATING.name + ".average", DocumentBuilder.gte(bondThreshold).get())
                .get()

        return DataAccess.fromCollection(Collection.PERCEPTIONS).find(query).map { doc -> doc.getString(PerceptionProperty.OTHER_ID.name) }.filter{
            Sentibyte.getAncestors(it, 3).any{
                if (ancestors.contains(it.key))
                {
                    // S' with a common T1 ancestor are cousins. S' with a common T2 ancestor share a great grandparent
                    val ancestorTier = ancestors[it.key]!!
                    val candidateAncestorTier = it.value
                    ancestorTier >= 3 && candidateAncestorTier >= 3
                }
                else
                {
                    // this candidate does not share a common ancestor
                    true
                }
            }
            // if any otherAncestors match ancestors, and the tier is <2, invalid
        }.toList()
    }

    private fun getPerception(otherId : String) : Perception
    {
        return perceptionCache.getOrPut(otherId){
            val toAdd = Perception.getPerception(sentibyteId, otherId)
            modified = true
            toAdd
        }
    }

    private fun getUniqueMemoryIds() : List<String>
    {
        val query = DocumentBuilder(InteractionKey.OWNER_ID.name, documentId).get()
        return DataAccess.fromCollection(Collection.INTERACTIONS)
                .find(query)
                .distinctBy{document -> document.getString(InteractionKey.OTHER_ID.name) }
                .map{document -> document.getString(InteractionKey.OTHER_ID.name)}
                .toList()
    }

    private fun updateContacts()
    {
        updateFriends()
        updateBonds()
    }

    private fun updateFriends()
    {
        val previousFriends = friends.toList()
        val query = DocumentBuilder.nin(Sentibyte.getAncestorList(sentibyteId, 3))
                .wrap(PerceptionProperty.OTHER_ID.name)
                .put(PerceptionProperty.OWNER_ID.name, documentId)
                .get()

        val sort = DocumentBuilder(PerceptionProperty.OVERALL_RATING.name + ".average", -1).get()

        friends = DataAccess.fromCollection(Collection.PERCEPTIONS).find(query).sort(sort).limit(FRIEND_COUNT)
                .map{it.getString(PerceptionProperty.OTHER_ID.name)}.toMutableSet()

        modified = !friends.equals(previousFriends)
    }

    private fun updateBonds()
    {
        val childAge = Community.getCommunity(communityId!!).childAge
        val maxChildren = Community.getCommunity(communityId!!).maxChildren

        if (age >= childAge && children.size < maxChildren)
        {
            val previousBonds = bonds.toMap()
            val qualifyingPerceptions = getBondCandidates()

            if (qualifyingPerceptions.isEmpty())
            {
                return
            }

            // TODO writeToDB these to be more significant
            val maxAge = Int.MAX_VALUE
            val minAge = childAge

            val ancestors = Sentibyte.getAncestorList(sentibyteId, 3)

            val query = DocumentBuilder()
                    // '_id': {$in: qualifyingPerceptions, $nin: family, $ne: documentId}
                    .putId(DocumentBuilder.iN(qualifyingPerceptions)
                            .putNin(ancestors)
                            .putNe(documentId)
                            .get())
                    // 'age': {$lt: MAX_AGE, $gt: MIN_AGE}
                    .put(SentibyteProperty.AGE.name, DocumentBuilder.lte(maxAge).putGte(minAge).get())
                    // 'children': {$size: {$lt: MAX_CHILDREN}}
                    .put(SentibyteProperty.CHILDREN.name, DocumentBuilder.lt(maxChildren).wrapSize().get())
                    .get()

            bonds = DataAccess.fromCollection(Collection.SENTIBYTES).find(query).limit(BOND_COUNT).map{
                val otherId = it.getString(DBKey.ID.key)!!
                val bondCycles = bonds.getOrDefault(otherId, 0)
                Pair(otherId, bondCycles)
            }.toMap().toMutableMap()

            modified = previousBonds.keys == bonds.keys
        }
    }

    fun addChild(otherId: String)
    {
        children.add(otherId)
        modified = true
        save()
    }
}