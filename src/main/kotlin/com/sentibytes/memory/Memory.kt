//package com.sentibytes.memory
//
//import com.mongodb.BasicDBList
//import com.sentibytes.infrastructure.DocumentBase
//import com.sentibytes.communication.Interaction
//import com.sentibytes.infrastructure.Collection
//import com.sentibytes.infrastructure.DataAccess
//import com.sentibytes.infrastructure.DocumentBuilder
//import org.bson.Document
//import org.bson.types.String
//import java.util.*
//
//class Memory(document: Document) : DocumentBase(document, Collection.INTERACTIONS)
//{
//    private val ownerId: String = fromString(MemoriesKey.OWNER_ID.name)
//
//    private val otherId: String = fromString(MemoriesKey.OTHER_ID.name)
//
//    private val interactions: MutableList<Interaction> = getList(MemoriesKey.INTERACTIONS.name)
//
//    companion object
//    {
//        fun getMemory(ownerId: String, otherId: String) : Memory
//        {
//            val query = DocumentBuilder(MemoriesKey.OWNER_ID.name, ownerId)
//                    .put(MemoriesKey.OTHER_ID.name, otherId)
//                    .getOrPut()
//            val collection = DataAccess.fromCollection(Collection.INTERACTIONS)
//            val dbDocument =  collection.find(query).first()
//
//            return if (dbDocument == null)
//            {
//                val addDoc = DocumentBuilder(MemoriesKey.OWNER_ID.name, ownerId)
//                        .put(MemoriesKey.OTHER_ID.name, otherId)
//                        .put(MemoriesKey.INTERACTIONS.name, BasicDBList())
//                        .getOrPut()
//
//                collection.insertOne(addDoc)
//                getMemory(ownerId, otherId)
//            }
//            else
//            {
//                Memory(dbDocument)
//            }
//        }
//    }
//
//    override fun pack(): Document
//    {
//        TODO("interactions need to be packed")
//        return DocumentBuilder(MemoriesKey.INTERACTIONS.name, interactions).getOrPut()
//    }
//
//    override fun save()
//    {
//        writeToDB()
//    }
//
//    override fun unpack(document: Document)
//    {
//        // all pertinent variables can be set by base methods
//    }
//
//    fun addInteraction(interaction: Interaction)
//    {
//        interactions.add(interaction)
//        modified = true
//    }
//
//    fun getRandomInteraction() : Interaction
//    {
//        val randomIndex = Math.floor(Random().nextDouble() * interactions.size).toInt()
//        return interactions[randomIndex]
//    }
//}