package com.sentibytes

enum class ContactType
{
    STRANGERS, FRIENDS, CONTACTS
}