package com.sentibytes.perceptions

import com.sentibytes.infrastructure.DocumentBase
import com.sentibytes.attributes.SentibyteAttribute
import com.sentibytes.cache.SentibyteCache
import com.sentibytes.communication.Interaction
import com.sentibytes.communication.InteractionType
import com.sentibytes.infrastructure.Collection
import com.sentibytes.infrastructure.DataAccess
import com.sentibytes.infrastructure.DocumentBuilder
import com.sentibytes.util.AverageContainer
import org.bson.Document
import org.bson.types.ObjectId

class Perception private constructor(document: Document) : DocumentBase(document, Collection.PERCEPTIONS)
{
    private val otherId: String = getValue(PerceptionProperty.OTHER_ID.name)!!

    private val ownerId: String = getValue(PerceptionProperty.OWNER_ID.name)!!

    private var broadcastsObserved : Int = getValue(PerceptionProperty.BROADCASTS_OBSERVED.name)!!

    private var cyclesObserved : Int = getValue(PerceptionProperty.CYCLES_OBSERVED.name)!!

    private var rumorsHeard : Int = getValue(PerceptionProperty.RUMORS_HEARD.name)!!

    private var memoriesCounted : Int = getValue(PerceptionProperty.MEMORIES_COUNTED.name)!!

    private var rating : AverageContainer = AverageContainer(getValue<Document>(PerceptionProperty.OVERALL_RATING.name)!!)

    private var averageDesiredOffset : AverageContainer = AverageContainer(getValue<Document>(PerceptionProperty.AVERAGE_DESIRED_OFFSET.name)!!)

    private var averageInteractionRating : AverageContainer = AverageContainer(getValue<Document>(PerceptionProperty.AVERAGE_INTERACTION_RATING.name)!!)

    private var averageInstanceRating : AverageContainer = AverageContainer(getValue<Document>(PerceptionProperty.AVERAGE_INSTANCE_RATING.name)!!)

    companion object
    {
        fun getPerception(ownerId: String, otherId: String) : Perception
        {
            val query = DocumentBuilder(PerceptionProperty.OWNER_ID.name, ownerId)
                    .put(PerceptionProperty.OTHER_ID.name, otherId)
                    .get()
            val collection = DataAccess.fromCollection(Collection.PERCEPTIONS)
            val dbDocument =  collection.find(query).first()
            if (dbDocument == null)
            {
                val regard = SentibyteCache.get().getOrPut(ownerId).attributes[SentibyteAttribute.REGARD]!!.getCurrent()
                val ratingBaseAverageContainer = AverageContainer()
                ratingBaseAverageContainer.addValue(regard)
                val addDoc = DocumentBuilder.id(ObjectId().toString())
                        .put(PerceptionProperty.OTHER_ID.name, otherId)
                        .put(PerceptionProperty.OWNER_ID.name, ownerId)
                        .put(PerceptionProperty.BROADCASTS_OBSERVED.name, 0)
                        .put(PerceptionProperty.CYCLES_OBSERVED.name, 0)
                        .put(PerceptionProperty.RUMORS_HEARD.name, 0)
                        .put(PerceptionProperty.MEMORIES_COUNTED.name, 0)
                        .put(PerceptionProperty.AVERAGE_DESIRED_OFFSET.name, AverageContainer.getBlankDBDoc())
                        .put(PerceptionProperty.AVERAGE_INTERACTION_RATING.name, AverageContainer.getBlankDBDoc())
                        .put(PerceptionProperty.AVERAGE_INSTANCE_RATING.name, AverageContainer.getBlankDBDoc())
                        .put(PerceptionProperty.OVERALL_RATING.name, ratingBaseAverageContainer.toDoc())
                        .get()

                collection.insertOne(addDoc)
                return getPerception(ownerId, otherId)
            }
            else
            {
                return Perception(dbDocument)
            }
        }
    }

    override fun pack() : Document
    {
        return DocumentBuilder()
                .put(PerceptionProperty.OTHER_ID.name, otherId)
                .put(PerceptionProperty.OWNER_ID.name, ownerId)
                .put(PerceptionProperty.BROADCASTS_OBSERVED.name, broadcastsObserved)
                .put(PerceptionProperty.CYCLES_OBSERVED.name, cyclesObserved)
                .put(PerceptionProperty.RUMORS_HEARD.name, rumorsHeard)
                .put(PerceptionProperty.MEMORIES_COUNTED.name, memoriesCounted)
                .put(PerceptionProperty.AVERAGE_DESIRED_OFFSET.name, averageDesiredOffset.toDoc())
                .put(PerceptionProperty.AVERAGE_INTERACTION_RATING.name, averageInteractionRating.toDoc())
                .put(PerceptionProperty.AVERAGE_INSTANCE_RATING.name, averageInstanceRating.toDoc())
                .put(PerceptionProperty.OVERALL_RATING.name, rating.toDoc())
                .get()
    }

    override fun save()
    {
        writeToDB()
    }

    fun addInteraction(interaction: Interaction, interactionType: InteractionType)
    {
        modified = true

        when(interactionType)
        {
            InteractionType.MEMORY -> memoriesCounted++
            InteractionType.RUMOR -> rumorsHeard++
            else ->
            {
                broadcastsObserved++
                cyclesObserved++
            }
        }

        if (interaction.traitGuesses.isNotEmpty())
        {
            val localAverageDesiredOffset = AverageContainer()
            val averageTraitRating = AverageContainer()

            // TODO make autocloseable
            val owner = SentibyteCache.get().getOrPut(ownerId)

            interaction.traitGuesses.forEach{
                val desiredOffset = Math.abs(owner.getCurrent(it.key) - it.value)
                localAverageDesiredOffset.addValue(desiredOffset)

                val tolerance = owner.getCurrent(SentibyteAttribute.TOLERANCE)
                val toleranceOffset = Math.abs(tolerance - desiredOffset)
                val traitRating = toleranceOffset/tolerance
                val traitPriority = owner.desiredAttributes.get(it.key)!!.priority

                averageTraitRating.addAverage(traitRating, traitPriority)
            }

            averageInteractionRating.addValue(averageTraitRating.average)
            averageDesiredOffset.addValue(localAverageDesiredOffset.average)
            rating.addValue(averageTraitRating.average)
        }
    }

    fun addInstance(value: Double)
    {
        TODO("implement")
    }

    fun getRating() : Double
    {
        return rating.average
    }
}