package com.sentibytes

import com.sentibytes.cache.SessionCache
import com.sentibytes.community.CommunityCache
import com.sentibytes.infrastructure.Collection
import com.sentibytes.infrastructure.DBKey
import com.sentibytes.infrastructure.DataAccess
import com.sentibytes.util.MockDBUtil
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SentibytesApplication

fun main(args: Array<String>)
{
    runApplication<SentibytesApplication>(*args)

//    MockDBUtil.clearAllDbs()

//    MockDBUtil.clearDB(Collection.KNOWLEDGE)
//    MockDBUtil.populate(Collection.KNOWLEDGE)

//    MockDBUtil.clearDB(Collection.ATTRIBUTES)
//    MockDBUtil.populate(Collection.ATTRIBUTES)

//    MockDBUtil.clearDB(Collection.COMMUNITIES)
//    MockDBUtil.populate(Collection.COMMUNITIES)


    MockDBUtil.clearDB(Collection.SESSIONS)
    MockDBUtil.clearDB(Collection.INTERACTIONS)

    MockDBUtil.clearDB(Collection.SENTIBYTES)
    MockDBUtil.populate(Collection.SENTIBYTES)

    MockDBUtil.clearDB(Collection.SENTIBYTE_METADATA)
    MockDBUtil.populate(Collection.SENTIBYTE_METADATA)

    MockDBUtil.clearDB(Collection.PERCEPTIONS)

    val community = CommunityCache.get(DataAccess.fromCollection(Collection.COMMUNITIES).find().first()!!.getString(DBKey.ID.key))
    for (i in 0..200)
    {
        community.cycle()
    }

    SessionCache.get().unloadAll()
}
