package com.sentibytes.infrastructure

enum class DBKey(val key: String)
{
    ID("_id"),
    SORT("\$sort"),
    MATCH("\$match"),
    NE("\$ne"),
    GT("\$gt"),
    LT("\$lt"),
    NIN("\$nin"),
    IN("\$in"),
    LTE("\$lte"),
    GTE("\$gte"),
    SIZE("\$size"),
    SET("\$set"),
    UNSET("\$unset")
}