package com.sentibytes.infrastructure

import com.sentibytes.infrastructure.DBKey.*
import org.bson.Document

class DocumentBuilder(initialDocument: Document)
{
    private var document: Document = initialDocument

    companion object
    {
        fun id(value: Any) : DocumentBuilder
        {
            return keyBuilder(ID, value)
        }

        fun sort(value: Any) : DocumentBuilder
        {
            return keyBuilder(SORT, value)
        }

        fun match(value: Any) : DocumentBuilder
        {
            return keyBuilder(MATCH, value)
        }

        fun ne(value: Any) : DocumentBuilder
        {
            return keyBuilder(NE, value)
        }

        fun gt(value: Any) : DocumentBuilder
        {
            return keyBuilder(GT, value)
        }

        fun lt(value: Any) : DocumentBuilder
        {
            return keyBuilder(LT, value)
        }

        fun nin(value: Any) : DocumentBuilder
        {
            return keyBuilder(NIN, value)
        }

        fun iN(value: Any) : DocumentBuilder
        {
            return keyBuilder(IN, value)
        }

        fun lte(value: Any) : DocumentBuilder
        {
            return keyBuilder(LTE, value)
        }

        fun gte(value: Any) : DocumentBuilder
        {
            return keyBuilder(GTE, value)
        }

        fun size(value: Any) : DocumentBuilder
        {
            return keyBuilder(SIZE, value)
        }

        fun set(value: Any) : DocumentBuilder
        {
            return keyBuilder(SET, value)
        }

        fun unset(value: Any) : DocumentBuilder
        {
            return keyBuilder(UNSET, value)
        }

        private fun keyBuilder(dbKey: DBKey, value: Any) : DocumentBuilder
        {
            return DocumentBuilder(dbKey.key, value)
        }
    }

    constructor() : this(Document())

    constructor(key: String, value: Any) : this()
    {
        put(key, value)
    }

    fun put(key: String, value: Any?) : DocumentBuilder
    {
        val parsedVal = when (value)
        {
            is String -> value.toString()
            is DocumentBuilder -> value.get()
            else -> value
        }

        document[key] = parsedVal
        return this
    }

    fun putId(value: Any) : DocumentBuilder
    {
        this.put(ID.key, value)
        return this
    }

    fun putSort(value: Any) : DocumentBuilder
    {
        this.put(SORT.key, value)
        return this
    }

    fun putMatch(value: Any) : DocumentBuilder
    {
        this.put(MATCH.key, value)
        return this
    }

    fun putNe(value: Any) : DocumentBuilder
    {
        this.put(NE.key, value)
        return this
    }

    fun putGt(value: Any) : DocumentBuilder
    {
        this.put(GT.key, value)
        return this
    }

    fun putLt(value: Any) : DocumentBuilder
    {
        this.put(LT.key, value)
        return this
    }

    fun putNin(value: Any) : DocumentBuilder
    {
        this.put(NIN.key, value)
        return this
    }

    fun putIn(value: Any) : DocumentBuilder
    {
        this.put(IN.key, value)
        return this
    }

    fun putLte(value: Any) : DocumentBuilder
    {
        this.put(LTE.key, value)
        return this
    }

    fun putGte(value: Any) : DocumentBuilder
    {
        this.put(GTE.key, value)
        return this
    }

    fun putSize(value: Any) : DocumentBuilder
    {
        this.put(SIZE.key, value)
        return this
    }

    fun putSet(value: Any) : DocumentBuilder
    {
        this.put(SET.key, value)
        return this
    }

    fun putUnset(value: Any) : DocumentBuilder
    {
        this.put(UNSET.key, value)
        return this
    }

    fun get() : Document
    {
        return document
    }

    fun toJson() : String{
        return document.toJson()
    }

    /**
     * Wraps entire initialDocument in an outer initialDocument with specified key
     *
     * for a initialDocument.....
     * {
     *      'thing': {
     *          'nested': "some kind of content"
     *      }
     * }
     *
     * running wrap("wrapper").....
     * {
     *      'wrapper': {
     *          'thing': {
     *              'nested': "some kind of content"
     *          }
     *      }
     * }
     *
     */
    fun wrap(wrappingKey: String) : DocumentBuilder
    {
        val wrapper = Document()
        wrapper[wrappingKey] = document
        document = wrapper
        return this
    }

    fun wrapId() : DocumentBuilder
    {
        this.wrap(ID.key)
        return this
    }

    fun wrapSort() : DocumentBuilder
    {
        this.wrap(SORT.key)
        return this
    }

    fun wrapMatch() : DocumentBuilder
    {
        this.wrap(MATCH.key)
        return this
    }

    fun wrapNe() : DocumentBuilder
    {
        this.wrap(NE.key)
        return this
    }

    fun wrapGt() : DocumentBuilder
    {
        this.wrap(GT.key)
        return this
    }

    fun wrapLt() : DocumentBuilder
    {
        this.wrap(LT.key)
        return this
    }

    fun wrapNin() : DocumentBuilder
    {
        this.wrap(NIN.key)
        return this
    }

    fun wrapIn() : DocumentBuilder
    {
        this.wrap(IN.key)
        return this
    }

    fun wrapLte() : DocumentBuilder
    {
        this.wrap(LTE.key)
        return this
    }

    fun wrapGte() : DocumentBuilder
    {
        this.wrap(GTE.key)
        return this
    }

    fun wrapSize() : DocumentBuilder
    {
        this.wrap(SIZE.key)
        return this
    }

    fun wrapSet() : DocumentBuilder
    {
        this.wrap(SET.key)
        return this
    }

    fun wrapUnset() : DocumentBuilder
    {
        this.wrap(UNSET.key)
        return this
    }
}