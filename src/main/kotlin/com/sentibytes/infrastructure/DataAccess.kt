package com.sentibytes.infrastructure

import com.mongodb.client.MongoClients
import com.mongodb.client.MongoCollection
import com.mongodb.client.MongoDatabase
import org.bson.Document

class DataAccess
{
    companion object
    {
        private val DB_CONNECTION_STRING = System.getProperty("dbConnectionString")
        private val DB_NAME = System.getProperty("dbName")
        private val MONGO_CLIENT = MongoClients.create(DB_CONNECTION_STRING)
        private val MONGO_DATABASE = MONGO_CLIENT.getDatabase(DB_NAME)

        fun getDatabase() : MongoDatabase {
            return MONGO_DATABASE
        }

        fun fromCollection(collection: Collection) : MongoCollection<Document>
        {
            return MONGO_DATABASE.getCollection(collection.name.toLowerCase())
        }

        fun get(id: String, collection: Collection) : Document?
        {
            val query = DocumentBuilder.id(id).get()
            return fromCollection(collection).find(query).first()
        }
    }
}