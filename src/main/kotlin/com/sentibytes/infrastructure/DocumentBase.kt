package com.sentibytes.infrastructure

import org.bson.Document
import java.util.ArrayList

abstract class DocumentBase(protected val document: Document, val collection: Collection)
{
    protected val documentId = document.getString(DBKey.ID.key)!!

    private var modifiedField = false
    protected var modified : Boolean
        get()
        {
            return modifiedField
        }
        set(value)
        {
            modifiedField = modifiedField || value
        }

    private val queryDocument : Document = DocumentBuilder.id(document.getString(DBKey.ID.key)).get()

    protected fun writeToDB()
    {
        if (modified)
        {
            val packedData = pack()
            packedData.remove(DBKey.ID.key)
            val updateDocument = DocumentBuilder.set(packedData).get()
            DataAccess.fromCollection(collection).updateOne(queryDocument, updateDocument)

            modifiedField = false
        }
    }

    protected abstract fun pack() : Document
    abstract fun save()

    protected fun <T> getSet(key: String) : MutableSet<T>
    {
        val localSet : MutableSet<T> = HashSet()
        val documentSet = document[key] as ArrayList<T>
        documentSet.forEach{
            localSet.add(it)
        }
        return localSet
    }

    protected fun <T> getList(key: String) : MutableList<T>
    {
        return document[key] as ArrayList<T>
    }

    protected fun <T, O> getMap(key: String, keyConverter: (String) -> T, valueConverter: (Any) -> O) : MutableMap<T, O>
    {
        val localMap : MutableMap<T, O> = HashMap()
        val documentMap : Document = document[key] as Document
        documentMap.toMap().forEach{
            localMap.put(keyConverter.invoke(it.key), valueConverter.invoke(it.value))
        }
        return localMap
    }

    protected fun <T, O> getHashMap(key: String, keyConverter: (String) -> T, valueConverter: (Any) -> O) : HashMap<T, O>
    {
        return HashMap(getMap(key, keyConverter, valueConverter))
    }

    protected fun <T> getValue(key: String) : T?
    {
        val value = document[key]
        return if (value == null) null else value as T
    }
}