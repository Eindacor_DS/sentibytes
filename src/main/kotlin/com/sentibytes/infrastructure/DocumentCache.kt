package com.sentibytes.infrastructure

interface DocumentCache <T : DocumentBase>
{
    fun getOrPut(id: String) : T

    fun get(id: String) : T?

    fun getWithoutPut(id: String) : T

    fun unload(id: String)

    fun unloadAll()
}