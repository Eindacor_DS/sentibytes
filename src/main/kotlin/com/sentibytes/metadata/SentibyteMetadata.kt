package com.sentibytes.metadata

import com.sentibytes.infrastructure.Collection
import com.sentibytes.infrastructure.DocumentBase
import com.sentibytes.infrastructure.DocumentBuilder
import org.bson.Document
import org.bson.types.ObjectId

class SentibyteMetadata(document: Document) : DocumentBase(document, Collection.SENTIBYTE_METADATA)
{
    private val sentibyteId: String = getValue("SENTIBYTE_ID")!!

    private val metadata: MutableMap<SentibyteMetadataCategory, Int> = HashMap()

    override fun save()
    {
        writeToDB()
    }

    init
    {
        SentibyteMetadataCategory.values().forEach{
            metadata[it] = getValue(it.name)!!
        }
    }

    override fun pack(): Document
    {
        val document = DocumentBuilder()
        metadata.forEach{
            document.put(it.key.name, it.value)
        }
        return document.get()
    }

    companion object
    {
        fun getBlank(sentibyteId: String) : Document
        {
            val document = DocumentBuilder.id(ObjectId().toString()).put("SENTIBYTE_ID", sentibyteId)

            SentibyteMetadataCategory.values().forEach {
                document.put(it.name, 0)
            }

            return document.get()
        }
    }

    fun increment(category: SentibyteMetadataCategory, count: Int)
    {
        metadata[category] = metadata[category]!! + count
        modified = true
    }

    fun increment(category: SentibyteMetadataCategory)
    {
        increment(category, 1)
    }
}