package com.sentibytes.knowledge

import com.sentibytes.infrastructure.Collection
import com.sentibytes.infrastructure.DBKey
import com.sentibytes.infrastructure.DataAccess
import com.sentibytes.util.Util
import org.bson.Document

class KnowledgeBase
{
    companion object
    {
        val TRUTH_SIZE = 1000
        var truthField: Map<String, String>? = null
        val theTruth : Map<String, String>
            get()
            {
                if (truthField == null)
                {
                    truthField = DataAccess.fromCollection(Collection.KNOWLEDGE).find(Document()).map {
                        Pair<String, String>(it.getString(DBKey.ID.key), it.getString(KnowledgeKey.VALUE.name))
                    }.toMap()
                }

                return truthField!!
            }

        fun getRandomTruth() : Pair<String, String>
        {
            return Util.randomElement(theTruth.toList())!!
        }

        fun getTruth(String: String) : Pair<String, String>
        {
            return Pair(String, theTruth[String]!!)
        }

        fun getRandomFalseTruth() : Pair<String, String>
        {
            val randomTruth = getRandomTruth()
            return falsify(randomTruth)
        }

        private fun falsify(knowledge: Pair<String, String>) : Pair<String, String>
        {
            TODO("change something about the value")
        }

        fun getRandomUnknown(known: MutableSet<String>) : Pair<String, String>?
        {
            val unknown = theTruth.filter { pair -> pair.key !in known }.toList()
            return Util.randomElement(unknown)
        }

        fun getRandomFalseUnknown(falseKnown: MutableSet<String>) : Pair<String, String>?
        {
            val randomUnknown = getRandomUnknown(falseKnown)

            return if (randomUnknown != null)
            {
                falsify(randomUnknown)
            }
            else
            {
                null
            }
        }
    }
}